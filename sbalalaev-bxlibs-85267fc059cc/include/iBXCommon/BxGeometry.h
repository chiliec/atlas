//
//  BxGeometry.h
//  iBXCommon
//
//  Created by Balalaev Sergey on 12/5/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#ifndef iBXCommon_BxGeometry_h
#define iBXCommon_BxGeometry_h

#include <CoreGraphics/CoreGraphics.h>

CG_INLINE CGPoint CGRectCenterPoint(CGRect rect);

CG_INLINE CGPoint CGRectCenterPoint(CGRect rect)
{
    return CGPointMake(rect.origin.x + rect.size.width / 2.0f, rect.origin.y + rect.size.height / 2.0f);
}

#endif
