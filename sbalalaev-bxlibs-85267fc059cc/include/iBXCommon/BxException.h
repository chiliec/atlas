//
//  BxException.h
//  iBXCommon
//
//  Created by Balalaev Sergey on 7/4/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BxException : NSException {
}

//! содержание ошибки, полученое из источника
@property (nonatomic, retain) NSError *error;

//! ручная инициализация
- (id) initWith: (NSError *) mainError;

//! получить самоудаляемый экземпляр
+ (id) exceptionWith: (NSError *) mainError;

@end
