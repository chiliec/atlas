//
// Created by Balalaev Sergey on 1/12/15.
// Copyright (c) 2015 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (BxUtils)

//! Методы работы с восстанавливаемыми данными, исключений не вызывают
- (BOOL) hasSkipBackupAttribute;
- (BOOL) addSkipBackupAttribute;
- (BOOL) removeSkipBackupAttribute;

@end