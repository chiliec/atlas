//
//  NSData+BxUtils.h
//  iBXCommon
//
//  Created by Balalaev Sergey on 7/8/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (BxUtils)

+ (NSData *) dataWithBase64EncodedString:(NSString *) string;
- (id) initWithBase64EncodedString:(NSString *) string;

- (NSString *) base64Encoding;
- (NSString *) base64EncodingWithLineLength:(unsigned int) lineLength;

@end
