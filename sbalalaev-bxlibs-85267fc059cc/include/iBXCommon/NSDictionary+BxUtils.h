//
//  NSDictionary+BxUtils.h
//  iBXCommon
//
//  Created by Balalaev Sergey on 9/3/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (BxUtils)

//! Получение строки в формате x-www-forms
- (NSString*) xFormsString;

@end
