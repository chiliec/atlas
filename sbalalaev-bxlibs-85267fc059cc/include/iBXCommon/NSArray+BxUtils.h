//
//  NSArray+BxUtils.h
//  iBXCommon
//
//  Created by Balalaev Sergey on 10/9/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef id (^BxArrayTransformFromDictionaryHandler) (NSDictionary * data);

@interface NSArray (BxUtils)

//! Получение строки в формате x-www-forms
- (NSString*) xFormsString;

- (NSArray*) arrayByTransformHandler: (BxArrayTransformFromDictionaryHandler) transformHandler;

@end

@interface NSMutableArray (BxUtils)

- (void) transformHandler: (BxArrayTransformFromDictionaryHandler) transformHandler;

@end
