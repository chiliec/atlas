//
//  BxUIUtils.h
//  iBXCommon
//
//  Created by Balalaev Sergey on 8/29/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BxUIUtils : NSObject

//! Возвращает самое главное представление для текущего
+ (UIView*) getSuperParentView: (UIView*) view;

//! Возвращает размер занимаемый во view клавиатурой
+ (CGRect) getKeyboardFrameIn: (UIView *) view userInfo: (NSDictionary*) userInfo;

@end
