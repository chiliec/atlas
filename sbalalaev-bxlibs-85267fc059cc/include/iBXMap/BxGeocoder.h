//
//  BxGeocoder.h
//  iBXMap
//
//  Created by Balalaev Sergey on 7/24/13.
//  Copyright (c) 2013 Byterix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class BxGeocoderData;

typedef void(^BxGeocoderHandler)(NSArray * result, NSString * errorMesage, BxGeocoderData * data);

@interface BxGeocoderData : NSObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) BxGeocoderHandler handler;

- (void) completionWithResult: (NSArray*) result;
- (void) completionWithErrorMessage: (NSString*) errorMessage;

- (void) cancel;

@end

@interface BxResultGeocoder : NSObject
{
    
}
- (id) initWithAddress: (NSString*) address coordinate: (CLLocationCoordinate2D) coordinate;

@property (nonatomic, readonly) NSString * address;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

@end

@interface BxGeocoder : NSObject
{
@protected
    NSMutableSet * allRequests;
}

- (BxGeocoderData *) startGeocodingWithAdress: (NSString*) address completionHandler: (BxGeocoderHandler) completionHandler;
- (BxGeocoderData *) startReverseGeocodingWithCoordinate: (CLLocationCoordinate2D) coordinate completionHandler: (BxGeocoderHandler) completionHandler;

- (void) allCancel;

@end
