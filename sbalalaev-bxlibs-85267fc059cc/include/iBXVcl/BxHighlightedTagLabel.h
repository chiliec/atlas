//
//  HighlightedTagLabel.h
//  iPadPravoRu
//
//  Created by iMac on 03.08.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BxHighlightedLabel.h"

@interface BxHighlightedTagLabel : BxHighlightedLabel {
    
}

- (id) initWithWidth: (CGFloat) labelWidth 
			position: (CGPoint) position 
				font: (UIFont*) font
				text: (NSString*) text 
			startTag: (NSString*) startTag 
			 stopTag: (NSString*) stopTag;

- (id) initWithWidth: (CGFloat) labelWidth
			position: (CGPoint) position
				font: (UIFont*) font1
            markfont: (UIFont*) markfont1
				text: (NSString*) text1
			startTag: (NSString*) startTag
			 stopTag: (NSString*) stopTag;

@end
