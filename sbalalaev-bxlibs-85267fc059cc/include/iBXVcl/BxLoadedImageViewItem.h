//
//  BxLoadedImageViewItem.h
//  iBXVcl
//
//  Created by Balalaev Sergey on 9/10/13.
//  Copyright (c) 2013 Byterix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BxData.h"
#import "BxViewItem.h"

@class BxLoadedImageViewItem;

typedef void (^BxLoadedImageViewItemHandler)(BxLoadedImageViewItem * view);

@interface BxLoadedImageViewItem : UIView <BxViewItem>
{
@protected
    UIImageView * _contentImage;
    UIActivityIndicatorView * _activityView;
    UIButton * _btAction;
}
@property (nonatomic, readonly) UIActivityIndicatorView * activityView;
@property (nonatomic, readonly) UIImageView * contentImage;
@property (nonatomic, copy) BxLoadedImageViewItemHandler actionHandler;
@property (nonatomic, copy) BxLoadedImageViewItemHandler loadedHandler;

+ (BxDownloadOldFileCasher*) casher;
+ (NSOperationQueue*) queue;
+ (UIImage*) notLoadedImage;

- (void) initObject;

- (void) setImageURL: (NSString*) url;

+ (UIImage*) cashedImageFromURL: (NSString*) url;

@end
