/**
 *	@file PhotoViewServiceController.h
 *	Просмотр фото из определенной области
 *	@date 01.11.2012
 *  @author Sergey Balalaev
 *	@warning Copyright 2012, Byterix. See http://byterix.net
 */

#import "BxServiceController.h"

@interface BxPhotoViewServiceController : BxServiceController
{
@protected
    UIImageView * photoView;
    CGRect startRect;
}

- (void) presentFromParent: (UIViewController*) parent imageView: (UIImageView*) imageView animated: (BOOL) animated;

@end
