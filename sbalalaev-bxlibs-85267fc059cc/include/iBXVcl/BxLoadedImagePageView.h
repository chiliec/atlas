//
//  BxLoadedImagePageView.h
//  iBXVcl
//
//  Created by Sergan on 09.10.13.
//  Copyright (c) 2013 Byterix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BxPageView.h"
#import "BxLoadedImageViewItem.h"

@interface BxLoadedImagePageView : BxPageView

@property (nonatomic, retain) BxLoadedImageViewItem *imageView;

@end
