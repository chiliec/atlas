//
//  SplashViewController.h
//  iByteriX
//
//  Created by Balalaev Sergey on 10/15/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import "BxVcl.h"

typedef void (^BxSplashAction)();

//! отображение сплеша
@interface BxSplashViewController : BxDialogController

@property (nonatomic, retain) UIImage * splashImage;
@property (nonatomic) NSTimeInterval time;
@property (nonatomic, copy) BxSplashAction action;

+ (void) checkFromParent: (UIViewController*) viewController image: (UIImage*) image time: (NSTimeInterval) time action: (BxSplashAction) action;
+ (void) checkFromParent: (UIViewController*) viewController;

//! этот метод можно переопределить для выполнения фоновых действий, по умолчанию выполняется блок action
- (void) execute;

@end
