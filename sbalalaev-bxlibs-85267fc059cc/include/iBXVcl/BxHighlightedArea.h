/**
 *	@file HighlightedArea.h
 */

#import <Foundation/Foundation.h>

//! тип выделения текста
typedef enum {
    HighlightTypeNone = 0,
    HighlightTypeColor = 1,
    HighlightTypeMarked = 2,
    HighlightTypeReserved = 4
} HighlightType;


@interface BxHighlightedArea : NSObject <NSCopying> {
@protected
	int position;
	int length;
    HighlightType type;
    BOOL isAreaInFocus;
}
@property (nonatomic) int position;
@property (nonatomic) int length;
@property (nonatomic) HighlightType type;
@property (nonatomic) BOOL isAreaInFocus;

+ (NSArray *) areasFromText: (NSString*) text
              highlightText: (NSString*) highlightText 
              highlightType: (HighlightType) highlightType;

+ (NSArray *) margedAareasFromFirst: (NSArray*) firstArray second: (NSArray*) secondArray;

- (id) initWithPosition: (int) position 
				 length: (int) length;

- (BxHighlightedArea*) intersectionWith: (BxHighlightedArea*) area;

- (NSRange) range;

- (BOOL) onExclusiveClick: (id) sender;

- (NSComparisonResult)comparePosition:(BxHighlightedArea *)otherObject;

@end
