//
//  BaseViewController.h
//  iPadPravoRu
//
//  Created by Sergey Balalaev
//

#import <UIKit/UIKit.h>

@interface BxBaseViewController : UIViewController

//! автоматически прячет навигационную панель и дает возможность показать ее в нужный момент
@property (nonatomic) BOOL autoHideNavigationBar;

//! при первом показе скрывает навигационную панель при autoHideNavigationBar = YES
@property (nonatomic) BOOL autoHideNavigationBarFromShow;

//! Тап на контролелере всегда можно обработать иначе, чем при выбранном autoHideNavigationBar == YES
- (void) tapAction;

@end
