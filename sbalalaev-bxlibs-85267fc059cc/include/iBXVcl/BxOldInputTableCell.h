//
//  InputTableCell.h
//

#import <UIKit/UIKit.h>

@class BxOldInputTableController;

@interface BxOldInputTableCell : UITableViewCell {
@protected
	UILabel * titleLable;
    UILabel * valueLabel;
    UILabel * subtitleLabel;
	BOOL isComment;
    CGFloat _height;
}
@property (nonatomic, readonly) UILabel * titleLable;
@property (nonatomic, readonly) UILabel * valueLabel;
@property (nonatomic, readonly) UILabel * subtitleLabel;
@property (nonatomic, assign) BxOldInputTableController * parent;

+ (id) cellFrom: (NSDictionary*) data parent: (BxOldInputTableController*) parent;

- (void) setComment: (NSString*) comment;

+ (CGFloat) tagWidth;

- (CGFloat) minValueWidth;

- (CGFloat) height;

- (void) update;

@end