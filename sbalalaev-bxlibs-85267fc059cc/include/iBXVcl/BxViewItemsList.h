//
//  BxViewItemsList.h
//  iBXVcl
//
//  Created by Balalaev Sergey on 9/10/13.
//  Copyright (c) 2013 Byterix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BxViewItem.h"

@class BxViewItemsList;
@class BxViewItemValue;
@class ItemsListPanGestureRecognizer;

// UITableView

@protocol BxViewItemsListSource <NSObject>

- (NSUInteger) numberOfLayersInContentItemScroll: (BxViewItemsList*) viewItemsList;

- (NSUInteger) viewItemsList: (BxViewItemsList*) viewItemsList numberOfItemsInLayer: (NSInteger) layer;

- (CGSize) viewItemsList: (BxViewItemsList*) viewItemsList sizeOfItemsInLayer: (NSUInteger) layer index: (NSUInteger) index;

- (UIView<BxViewItem>*) viewForViewItemsList: (BxViewItemsList*) viewItemsList layer: (NSUInteger) layer index: (NSUInteger) index;

@end

@protocol BxViewItemsListDelegate <NSObject>

@optional

- (void) viewItemsList: (BxViewItemsList*) viewItemsList changeLayer: (NSInteger) layer index: (NSInteger) index;

@end

typedef UIView<BxViewItem>* (^BxViewItemCreateHandler)();

typedef NS_ENUM(NSInteger, BxViewItemsListOrientation) {
    BxViewItemsListHorizontalOrientation,
    BxViewItemsListVerticalOrientation
};

@interface BxViewItemsList : UIView <UIGestureRecognizerDelegate>
{
@protected
    NSInteger layersCount;
    NSMutableArray * itemsValueLayers;
    NSMutableArray * bufferedItemValues;
    NSMutableDictionary * removedItemViews;
    NSInteger currentIndex;
    CGFloat currentShift;
    
    CGFloat allPosition;
    CGFloat endBorderedPosition;
    NSInteger endBorderedIndex;

    ItemsListPanGestureRecognizer * _panGestureRecognizer;
    NSInteger lastPadingPageIndex;
    CGSize lastSize;
}
@property (nonatomic, assign) id<BxViewItemsListSource> dataSource;
@property (nonatomic, assign) id<BxViewItemsListDelegate> delegate;
@property (nonatomic) NSInteger bufferMinCount;
@property (nonatomic) BOOL isCentred;
@property (nonatomic) BOOL isSticked;
@property (nonatomic) BOOL isScale;
@property (nonatomic) BOOL isVisualBordered;
@property (nonatomic) BOOL isCentredOfFull;
@property (nonatomic, readonly) NSInteger currentIndex;
@property (nonatomic) double inertial;
@property (nonatomic) BOOL enabled;
@property (nonatomic) BxViewItemsListOrientation orientation;
@property (nonatomic,retain) UIView * indicatorView;


- (void) updateWithIndex: (NSInteger) index;
- (void) updateWithoutRefreshWithIndex: (NSInteger) index;

- (void) refreshWithDeleteIndex: (NSUInteger) index animated: (BOOL) animated;
- (void) refreshWithAddIndex: (NSUInteger) index animated: (BOOL) animated;
- (void) refreshAnimated: (BOOL) animated;

- (UIView<BxViewItem>*)createBufferedViewWithIdentifier:(NSString *)identifier forLayer:(NSUInteger)layer index:(NSUInteger)index creator: (BxViewItemCreateHandler) creator;

- (BxViewItemValue*) getItemFromView: (UIView<BxViewItem>*) view;
- (UIView<BxViewItem>*) getViewFromIndex: (NSUInteger) index;

@end
