//
//  BxPageControl.h
//  iBXVcl
//
//  Created by Balalaev Sergey on 9/6/13.
//  Copyright (c) 2013 Byterix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BxPageControl : UIPageControl

@property (nonatomic, retain) UIImage * activeImage UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIImage * inactiveImage UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGFloat indention UI_APPEARANCE_SELECTOR;

@end
