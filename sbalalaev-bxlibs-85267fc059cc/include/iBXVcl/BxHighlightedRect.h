//
//  HighlightedRect.h
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BxHighlightedRect : NSObject {
}
@property (nonatomic) CGRect rect;

@end
