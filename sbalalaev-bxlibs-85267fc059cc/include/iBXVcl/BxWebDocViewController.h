/**
 *	@file BxWebDocViewController.h
 *	Контроллер отображения HTML содержимого
 *	@date 17.08.2012
 *  @author Sergey Balalaev
 *	@warning Copyright 2012, Byterix. See http://byterix.net
 */

#import <Foundation/Foundation.h>
#import "BxBaseViewController.h"
#import "MBProgressHUD.h"
#import "BxData.h"

@interface BxWebDocViewController : BxBaseViewController <UIWebViewDelegate> {
    
@protected
	UIWebView * content;
	MBProgressHUD * HUD;
	BOOL isLoad;
	NSString * url;
	BOOL isReload;
}

+ (BxDownloadOldFileCasher*) defaultFileCash;

//! Следующие методы открывают изображения сразу на webKit
- (void) setUrl: (NSString*) url1 isFit: (BOOL) isFit;
- (void) setHTML: (NSString*) html baseUrl: (NSString*) rawBaseUrl;
- (void) setUrl: (NSString*) url;
- (void) setPath: (NSString*) path;
- (void) setHTML: (NSString*) html;

//! Загрузка осуществляется через кешер
- (void) downloadWith: (NSString*) url1;

//! Метод обеспечивает синхранизацию загрузки контента
- (void) waitForLoading;



@end
