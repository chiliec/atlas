/**
 *	@file KeyboardController.h
 *	Контроллер подразумевающий использование клавиатуры
 *  @author Sergey Balalaev
 */

#import <Foundation/Foundation.h>
#import "BxBaseViewController.h"

@interface BxKeyboardController : BxBaseViewController {
@protected
	UIView * contentView;
	BOOL keyboardShown;
}
@property (nonatomic, readonly) UIView * contentView;

- (BOOL) isAlwaysShowKeyboard;
- (BOOL) isAlwaysHiddenKeyboard;
- (BOOL) isContentResize;

- (void) onWillChangeContentSizeWithShow: (BOOL) isKeyBoardWillShow;
- (void) onDidChangeContentSizeWithShow;

- (void)keyboardWasShown:(NSNotification*)aNotification;
- (void)keyboardWasHidden:(NSNotification*)aNotification;

@end
