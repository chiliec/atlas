//
// Created by Balalaev Sergey on 9/23/14.
// Copyright (c) 2014 Byterix. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    NormalItemStationingStyle = 0,
    LevelFixedItemStationingStyle = 1,
    TransformFixedItemStationingStyle = 2,
} ItemStationingStyle;

@protocol BxTreeListItemProtocol <NSObject>

//! например у section = 0, у cell = 1
- (int) level;
//! например у section = LevelFixedItemStationingStyle, у cell = NormalItemStationingStyle
- (ItemStationingStyle) stationingStyle;
//! поле необходимо для трансформации секций с определенной высоты
- (CGFloat) stationingHeight;
//! сдвиг эллемента вниз, если требуется (это для многослойных таблиц используется)
- (CGFloat) shiftY;
//! служебное поле, вызывается для изменения поведения в зависимости от перемещения по документу
- (void) toStationingY: (CGFloat) y;
//! Высота, с которой начинает сдвигать текущий элемент одноуровнего
- (CGFloat) startTransformHeight;

@end