//
//  highlightedTextInfo.h
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BxHighlightedArea.h"

@interface BxHighlightedTextInfo : NSObject <NSCopying> {
@protected
	NSString * text;
	int levelIndex;
	BxHighlightedArea * area;
	CGFloat x;
	CGFloat width;
}
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) BxHighlightedArea * area;
@property (nonatomic) int levelIndex;
@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat width;

- (id) initWithText: (NSString*) text;

@end
