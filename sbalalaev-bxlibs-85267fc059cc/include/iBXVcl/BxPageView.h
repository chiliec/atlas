//
//  BxPageView.h
//  iBXVcl
//
//  Created by Sergan on 09.10.13.
//  Copyright (c) 2013 Byterix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BxPageView : UIScrollView <UIScrollViewDelegate> {
    
}

- (void) setDefaultState;

@end