//
//  BxDatabase.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/5/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"

@interface BxDatabase : NSObject
{
@protected
    NSString * _fileName;
    BOOL _includeFromBackup;
}
@property (nonatomic, retain, readonly) FMDatabase * database;
@property (nonatomic, retain, readonly, getter = getPathForRestore) NSString * filePath;
//! Позволяет восстановить базу из iCloud. Внимание Apple не всех пропустит с этой функцией. По умолчанию отключена
@property (nonatomic, getter = _includeFromBackup, setter = setIncludeFromBackup:) BOOL includeFromBackup;

+ (BxDatabase*) default;

- (void) open;

/**
* fileName - название файла, в качестве которого может быть полный путь.
* название конечного файла будет использовано, чтобы достать из ресурсов проекта в случае
* отсутствия файла по указанному пути
*/
- (id) initWithFileName: (NSString*) fileName;

- (BOOL) executeWith: (NSString*) sql;

- (NSNumber*) executeNumberFunctionWith: (NSString*) sql;

- (NSArray*) allDataWith: (NSString*) sql;

- (NSString*)lastErrorMessage;

@end
