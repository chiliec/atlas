//
//  BxXmlDataParser.h
//  iBXData
//
//  Created by Balalaev Sergey on 8/27/14.
//  Copyright (c) 2014 ByteriX. All rights reserved.
//

#import "BxAbstractDataParser.h"
#import "XMLDictionary.h"

@interface BxXmlDataParser : BxAbstractDataParser

@end
