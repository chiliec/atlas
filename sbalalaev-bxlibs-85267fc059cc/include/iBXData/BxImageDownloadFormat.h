//
//  BxImageDownloadFormat.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/23/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    BxImageFormatJPG,
    BxImageFormatPNG
} BxImageFormat;

@interface BxImageDownloadFormat : NSObject

@property (nonatomic, retain) NSString * apendixName;
@property (nonatomic) CGSize size;
@property (nonatomic) BxImageFormat imageFormat;

- (id) initWithImageFormat: (BxImageFormat) imageFormat size: (CGSize) size;
- (id) initWithImageFormat: (BxImageFormat) imageFormat size: (CGSize) size apendixName: (NSString*) apendixName;

@end
