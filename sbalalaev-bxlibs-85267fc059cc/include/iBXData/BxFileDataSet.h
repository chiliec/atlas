//
//  BxResourceDataSet.h
//  iBXData
//
//  Created by Sergan on 04.05.14.
//  Copyright (c) 2014 ByteriX. All rights reserved.
//

#import "BxAbstractDataSet.h"
#import "BxAbstractDataParser.h"

@interface BxFileDataSet : BxAbstractDataSet
{
@protected
    //! парсер из текста в данные
    BxAbstractDataParser * _parser;
}

@property (nonatomic, strong) NSString * fileName;

- (id) initWithTarget: (id<BxAbstractDataSetDelegate>) target1
			   parser: (BxAbstractDataParser*) parser1;

- (id) initWithTarget: (id<BxAbstractDataSetDelegate>) target1
			   parser: (BxAbstractDataParser*) parser1
             fileName: (NSString*) fileName;

- (id) initWithTarget: (id<BxAbstractDataSetDelegate>) target1
			   parser: (BxAbstractDataParser*) parser1
     resourceFileName: (NSString*) resourceFileName;

@end
