//
//  BxTopUpdatedIdentifierDataCasher.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/10/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import "BxTopIdentifierDataCasher.h"

/**
 *  Менеджер кеширования данных с привязкой к идентификатору и
 *  удалением предыдущих сохранений при достижении лимита,
 *  так же способен к закачке из офлайн и отметки того что лучше обновиться из вне
 */
@interface BxTopUpdatedIdentifierDataCasher : BxTopIdentifierDataCasher

//! помечает элемент, как требующего обновления
- (NSDictionary *) setNotUpdatedWithIdentifier: (NSString*) value;

@end
