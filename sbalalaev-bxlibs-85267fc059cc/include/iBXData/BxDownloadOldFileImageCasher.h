//
//  BxDownloadOldFileImageCasher.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/23/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import "BxDownloadOldFileCasher.h"
#import "BxImageDownloadFormat.h"

@interface BxDownloadOldFileImageCasher : BxDownloadOldFileCasher
{
@protected
    NSMutableArray * _formats;
}

- (void) addFormat: (BxImageDownloadFormat*) format;
- (void) addImageFormat: (BxImageFormat) imageFormat size: (CGSize) size;

// formatIndex нумеруется начиная с 0. При 0 возвращается исходная картинка, а больше 0 берется из очереди картинок в соответствии с индексом добавленного формата
- (NSString *) getDownloadedPathFrom: (NSString*) url errorConnection: (BOOL) errorConnection progress: (id<BxDownloadProgress>) progress formatIndex: (int) formatIndex;
- (NSString*) getLocalDownloadedPathFrom: (NSString*) url formatIndex: (int) formatIndex;

@end
