//
//  BxDownloadStream.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/8/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BxException.h"
#import "BxDownloadProgress.h"
#import "BxDownloadUtils.h"


/**
 *	Исключение для закачки ресурсов из интернета
 */
@interface BxDownloadStreamException : BxException {
}
@end

typedef void (^BxDownloadStreamHandler)(NSError * error, NSData * data);

typedef void (^BxDownloadStreamHttpResponseHandler)(NSDictionary * allHeaderData);

@interface BxDownloadStream : NSObject <BxDownloadStreamProtocol>
{
@protected
	//! Прирост уровня загрузке
	float progressScale;
	//! Максимально возможный уровень загрузки
	float maxProgress;
	//! Интерфейс индикации уровня загрузки
	id<BxDownloadProgress> progress;
    //! Флаг необходим для корректной обработки отмены загрузки
    BOOL isCanceled;
@protected
	//! соединение для закачки данных
	NSURLConnection * urlDownload;
	//! закачиваемые данные
	NSMutableData * loadingData;
}

/**
 *	@brief Загрузка Данных из ресурса по ссылке url
 *	@throw DownloadException
 */
+ (NSData *) loadFromUrl: (NSString*) url;
+ (NSData *) loadFromUrl: (NSString*) url timeoutInterval:(NSTimeInterval)timeoutInterval;

/**
 *	@brief Загрузка Данных из ресурса по ссылке url с синхронным показом уровня загрузки
 *	@throw DownloadException
 */
+ (NSData *) loadFromUrl: (NSString*) url
             maxProgress: (float) maxProgress1
                delegate: (id<BxDownloadProgress>) delegate;

+ (NSData *) loadFromRequest: (NSURLRequest *) request
                 maxProgress: (float) maxProgress1
                    delegate: (id<BxDownloadProgress>) delegate;

+ (NSData *) loadFromRequest: (NSURLRequest *) request
                 maxProgress: (float) maxProgress1
                    delegate: (id<BxDownloadProgress>) delegate
                      stream: (BxDownloadStream **) stream;

/**
 *	@brief Возвращает информацию о ресурсе url
 *
 *	Происходит удаленный доступ синхронно
 *	@param url [in] - ссылка на ресурс (например html страницу)
 *	@return Справочник заголовка
 */
+ (NSDictionary *) getAllHeaderFieldsFrom: (NSString*)url;

//! Прирост загрузки данных до maxProgress при получении соединения с ресурсом response
+ (float) getProgressScaleWith: (NSURLResponse *)response maxProgress: (float) maxProgress1;

//! Старт загрузки данных по запросу
- (void) start: (NSURLRequest *) request responseHandler: (BxDownloadStreamHttpResponseHandler) responseHandler
       handler: (BxDownloadStreamHandler) handler;

//! Отмена операции загрузки
- (void) cancel;

@end
