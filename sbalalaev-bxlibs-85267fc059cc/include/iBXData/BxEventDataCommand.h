//
//  BxEventDataCommand.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/10/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import "BxAbstractDataCommand.h"

//! Команда, делегирующая событие
@interface BxEventDataCommand : BxAbstractDataCommand {
@protected
	NSObject * target;
	SEL selector;
}

- (id) initWithTarget: (NSObject*) terget1 selector: (SEL) selector1;

@end
