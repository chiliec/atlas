//
//  BxServiceDataSet.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/10/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import "BxAbstractDataSet.h"
#import "BxDownloadProgress.h"

@class BxDownloadStream;
@class BxAbstractDataParser;

@interface BxServiceDataSet : BxAbstractDataSet<BxDownloadProgress> {
@protected
	//! парсер из текста в данные
	BxAbstractDataParser * parser;
@protected
	NSString * url;
	NSString * post;
    BxDownloadStream * downloadSack;
}

@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * post;

- (id) initWithTarget: (id<BxAbstractDataSetDelegate>) target1
			   parser: (BxAbstractDataParser*) parser1;

//! @protected
- (void) updateRequest: (NSMutableURLRequest*) request;

//! эти методы помогают внести корректировки при необходимости, перед самой отсылкой
- (NSString*) getRequestUrl;
- (NSString*) getRequestPost;

@end