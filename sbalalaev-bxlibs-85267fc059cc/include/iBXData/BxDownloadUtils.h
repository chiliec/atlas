//
//  BxDownloadUtils.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/9/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BxDownloadProgress.h"

@protocol BxDownloadStreamProtocol

- (void) cancel;

@optional

+ (void) loadFromUrl: (NSString*) url
         maxProgress: (float) maxProgress1
            delegate: (id<BxDownloadProgress>) delegate
            filePath: (NSString*) filePath;

+ (NSData *) loadFromUrl: (NSString*) url
             maxProgress: (float) maxProgress1
                delegate: (id<BxDownloadProgress>) delegate;

@end

@interface BxDownloadUtils : NSObject

+ (void) setNetworkActivity: (BOOL) isNetworkActivity;

+ (NSURLRequest*) getRequestFrom: (NSString*) url;

@end
