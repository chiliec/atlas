//
//  BxExpiredDataCasher.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/10/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import "BxDataCasher.h"

#define ExpiredPeriodHour 3600.0
#define ExpiredPeriodDay 24.0 * ExpiredPeriodHour
#define ExpiredPeriodMonth 30.0 * ExpiredPeriodDay

FOUNDATION_EXPORT NSString const* FNExpiredDataCasherCashed;
FOUNDATION_EXPORT NSString const* FNExpiredDataCasherDate;

//! менеджер кеширования с заданным сроком действия информации
@interface BxExpiredDataCasher : BxDataCasher {
@protected
	NSDateFormatter * dateFormatter;
	//! срок действия данных в секундах
	NSTimeInterval expiredInterval;
}

//! конструктор с заданным интервалом срока действия в секундах
- (id) initWithFileName: (NSString*) cashedFileName1
		expiredInterval: (NSTimeInterval) expiredInterval1;

//! загрузка данных, даже если они неактуальны
- (NSDictionary *) loadDataIfExpired;

//! Дата последнего обновления кеша
- (NSDate*) cashDate;

@end
