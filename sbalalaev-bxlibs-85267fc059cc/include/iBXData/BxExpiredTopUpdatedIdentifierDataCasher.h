//
//  BxExpiredTopUpdatedIdentifierDataCasher.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/10/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import "BxTopUpdatedIdentifierDataCasher.h"
#import "BxExpiredDataCasher.h"

/**
 *	Менеджер кеширования данных имеющих срок действия и идентификацию
 *  с возможностью инвалидировать по идентификатору пул кеша
 */
@interface BxExpiredTopUpdatedIdentifierDataCasher : BxTopUpdatedIdentifierDataCasher
{
@private
    NSDateFormatter * dateFormatter;
	//! срок действия данных в секундах
	NSTimeInterval expiredInterval;
}

- (id) initWithFileName: (NSString*) cashedFileName1
               topCount: (int) topCount1
        expiredInterval: (NSTimeInterval) expiredInterval1;

//! загрузка данных, даже если они неактуальны
- (NSDictionary *) loadDataIfExpired;

//! Дата последнего обновления кеша
- (NSDate*) cashDate;

@end
