//
//  BxIdentifierDataCasher.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/10/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import "BxDataCasher.h"

//! Кеширование в отдельные файлы по указанному в названии файла кеша пути по идентификатору
@interface BxIdentifierDataCasher : BxDataCasher{
@protected
	//! главный путь, указанный пользователем для данного кешера
	NSString * mainFileName;
}
//! идентификатор кеша, в нем не должно быть символов "/" иначе чиститься не будет
@property (nonatomic, retain) NSString * identifier;

//! гарантированно и независимо сохраняет данные в кеше
- (void) saveDataFromExternal: (NSDictionary *) data;

//! удаляет из данного кеша, все что не найдено в списке идентификаторов
- (void) deleteAllNotInIdentifierList: (NSArray*) list;

//! удаляет из кеша данные по конкретному идентификатору
- (void) deleteWithIdentifier: (NSString*) value;

@end