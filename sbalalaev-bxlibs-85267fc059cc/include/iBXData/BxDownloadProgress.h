//
//  BxDownloadProgress.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/8/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *	Интерфейс изменения уровня загрузки
 */
@protocol BxDownloadProgress <NSObject>

//! Текущее значение уровня загрузки
- (float) getPosition;

//! Установка текущего уровня загрузки
- (void) setPosition: (float) position;

//! Запуск визуального представления моментальной загрузки
- (void) startFastFull;

//! Отвечает всем критериям гарантии, что индикатор готов отреагировать на изменения
- (BOOL) isActive;

@optional

- (void) setText: (NSString*) text;
- (NSString*) getText;

@end

//! Определение готовниости индикатора загрузки для работы с ним
BOOL DPisValid(id<BxDownloadProgress> downloadProgress);
//! Возврат значения индикатора загрузки
float DPgetPosition(id<BxDownloadProgress> downloadProgress);
//! Установка определенного значения индикатору загрузки
void DPsetPosition(id<BxDownloadProgress> downloadProgress, float value);
//! Прирощение определенного значения индикатору загрузки
void DPincPosition(id<BxDownloadProgress> downloadProgress, float value);
//! Визуализация быстрой загрузки
void DPstartFastFull(id<BxDownloadProgress> downloadProgress);