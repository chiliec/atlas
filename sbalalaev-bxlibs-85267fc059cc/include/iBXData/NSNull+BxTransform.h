//
//  NSNull+BxTransform.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/8/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNull (BxTransform)

- (NSString*) notNullString;
- (float) notNullFloat;
- (double) notNullDouble;
- (int) notNullInt;
- (BOOL) notNullBool;
- (NSArray*) notNullArray;
- (NSDictionary*) notNullDictionary;

- (BOOL) isNotNil;

@end
