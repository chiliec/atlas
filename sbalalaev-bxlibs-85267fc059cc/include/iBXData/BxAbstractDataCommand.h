//
//  BxAbstractDataCommand.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/10/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^BxDataCommandSuccessHandler)(id command);
typedef void (^BxDataCommandErrorHandler)(NSError * error);
typedef void (^BxDataCommandCancelHandler)();

extern NSString *const FNDataCommandName;

//! Абстрактная команда исполнения
@interface BxAbstractDataCommand : NSObject {
@protected
    BOOL isCanceled;
}
@property (getter = getCaption, nonatomic, readonly) NSString * caption;
@property (getter = getName, nonatomic, readonly) NSString * name;
@property (nonatomic, readonly) BOOL isCanceled;

- (NSMutableDictionary*) saveToData;
- (void) loadFromData: (NSMutableDictionary*) data;
- (void) execute;
- (void) updateFrom: (BxAbstractDataCommand*) command;

- (void)cancel;

//! Выполняет операцию в фоновом потоке, по завершению обрабатывает результат
- (void) executeWithSuccess: (BxDataCommandSuccessHandler) successHandler
               errorHandler: (BxDataCommandErrorHandler) errorHandler
              cancelHandler: (BxDataCommandCancelHandler) cancelHandler;

@end
