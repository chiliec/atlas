//
//  BxGroupDataCommand.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/10/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import "BxAbstractDataCommand.h"

@class BxQueueDataCommand;

@interface BxGroupDataCommand : BxAbstractDataCommand {
@protected
	NSMutableArray * commands;
	NSString * caption;
	BxQueueDataCommand * queue;
}

- (id) initWithQueue: (BxQueueDataCommand*) queue1 caption: (NSString*) caption1;

- (void) addCommand: (BxAbstractDataCommand*) command;

@end