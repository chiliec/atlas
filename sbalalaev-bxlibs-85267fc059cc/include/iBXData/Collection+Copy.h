/**
 *	@file Collection+Copy.h
 *	Коллекция копирования коллекций
 *	@date 01.08.2012
 *  @author Sergey Balalaev
 *	@warning Copyright 2012, Byterix. See http://byterix.net
 */

#import <Foundation/NSObject.h>
#import <Foundation/NSEnumerator.h>


@interface NSArray (Copy)

- (NSMutableArray*) getInstanceCopy;

@end

@interface NSDictionary (Copy)

- (NSMutableDictionary*) getInstanceCopy;

@end
