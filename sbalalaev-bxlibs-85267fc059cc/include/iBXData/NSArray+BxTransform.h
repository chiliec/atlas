//
//  NSArray+BxTransform.h
//  iBXData
//
//  Created by Balalaev Sergey on 10/25/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (BxTransform)

- (NSString*) notNullString;
- (float) notNullFloat;
- (double) notNullDouble;
- (int) notNullInt;
- (BOOL) notNullBool;
- (NSDictionary*) notNullDictionary;
- (NSArray*) notNullArray;

- (BOOL) isNotNil;

@end
