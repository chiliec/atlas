//
//  BxDownloadStreamWithResume.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/23/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BxDownloadUtils.h"

typedef enum {
    BxDownloadStreamWithResumeFileExistTypeNone,
    BxDownloadStreamWithResumeFileExistTypeError,
    BxDownloadStreamWithResumeFileExistTypeResume
} BxDownloadStreamWithResumeFileExistType;

@interface BxDownloadStreamWithResume : NSObject <BxDownloadStreamProtocol>
{
@protected
	//! Прирост уровня загрузке
	float progressScale;
	//! Максимально возможный уровень загрузки
	float maxProgress;
	//! Интерфейс индикации уровня загрузки
	id<BxDownloadProgress> progress;
@protected
	NSCondition * condition; // property
	//! соединение для закачки данных
	NSURLConnection * urlDownload;
	//! закачиваемые данные, которые будут сохранены в файл.
    NSString * filePath;
	NSOutputStream * fileStream;
	//! Ошибка возникшая в процессе закачки
	NSError * error;
    //
    NSDate * modifiedDate;
    //!
    unsigned long long startPosition;
    //
    NSMutableURLRequest * request;
    int dataPacketBytesCount;
}
@property (nonatomic, retain) NSString * filePath;
@property (nonatomic, retain) NSOutputStream * fileStream;
@property (nonatomic, retain) NSURLConnection * urlDownload;
@property (nonatomic, retain) NSError * error;
@property (nonatomic, retain) NSDate * modifiedDate;
@property (nonatomic, retain) NSMutableURLRequest * request;

+ (void) loadFromUrl: (NSString*) url
         maxProgress: (float) maxProgress1
            delegate: (id<BxDownloadProgress>) delegate
            filePath: (NSString*) filePath;

- (void) cancel;

@end
