//
//  BxServiceDataCommand.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/10/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import "BxAbstractDataCommand.h"
#import "BxDownloadStream.h"
#import "BxAbstractDataParser.h"

typedef enum
{
    BxServiceMethodGET,
    BxServiceMethodPOST,
    BxServiceMethodPUT,
    BxServiceMethodDELETE
} BxServiceMethod;

//! Команда, выполняющая действия на серевере, через веб-сервис
@interface BxServiceDataCommand : BxAbstractDataCommand{
@protected
    NSString * url;
	NSString * caption;
    NSString * requestBody;
	NSDictionary * result;
	NSDictionary * rawResult;
	NSMutableDictionary * data;
    BxServiceMethod method;
    BxDownloadStream * stream;
}
@property (nonatomic, readonly) NSString * url;
@property (nonatomic, readonly) NSDictionary * result;
@property (nonatomic, readonly) NSDictionary * rawResult;
@property (nonatomic, readonly) NSMutableDictionary * data;
@property (nonatomic, retain) BxAbstractDataParser * parser;

- (id) initWithUrl: (NSString*) url
              data: (NSDictionary*) data1
            method: (BxServiceMethod) method
           caption: (NSString*) caption1;

//! Если объект data1 пустой, то вызовется GET, иначе POST
- (id) initWithUrl: (NSString*) url
              data: (NSDictionary*) data1
           caption: (NSString*) caption1;

//! @private
- (void) updateRequest: (NSMutableURLRequest*) request;
//! @private
- (NSString *) getRequestBody;

- (NSDictionary*) checkResult: (NSDictionary*) dataResult;



@end