//
//  BxTopIdentifierDataCasher.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/10/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import "BxIdentifierDataCasher.h"

//! Менеджер кеширования данных с привязкой к идентификатору и  удалением предыдущих сохранений при достижении лимита
@interface BxTopIdentifierDataCasher : BxIdentifierDataCasher

@property (nonatomic) int topCount;

- (id) initWithFileName: (NSString*) cashedFileName1 topCount: (int) topCount1;

- (void) clear;

@end
