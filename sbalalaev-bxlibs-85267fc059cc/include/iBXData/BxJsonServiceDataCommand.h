//
//  BxJsonServiceDataCommand.h
//  iBXData
//
//  Created by Balalaev Sergey on 7/10/13.
//  Copyright (c) 2013 ByteriX. All rights reserved.
//

#import "BxServiceDataCommand.h"

//! Команда, которая общается с сервером на языке JSON
@interface BxJsonServiceDataCommand : BxServiceDataCommand

@end
