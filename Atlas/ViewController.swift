//
//  ViewController.swift
//  Atlas
//
//  Created by Бабин Владимир on 17/11/15.
//  Copyright © 2015 Babin. All rights reserved.
//

import UIKit
import AxUniversalMapKit

class ViewController: UIViewController {

    var map: AxMapKitMap!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        map = AxMapKitMap(frame: self.view.bounds)
        view.addSubview(map)
        loadDefaults()
    }
    
    private func loadDefaults() {
        map.minZoom = 9
        map.maxZoom = 18
    }

}
