//
//  AxNewMapMultilineObject.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 19.02.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "AxNewAbstractMapShapeObject.h"

@interface AxNewMapMultilineObject : AxNewAbstractMapShapeObject

- (void)updateLineWithLineColor:(UIColor *)multilineColor
                      lineWidth:(CGFloat)multilineWidth;

@end
