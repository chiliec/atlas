//
//  AxNewMapPointObject.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 19.02.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "AxNewMapObject.h"

typedef NS_ENUM(NSInteger, AxNewMapPointIconAlign) {
    AxNewMapPointIconAlignCenter,
    AxNewMapPointIconAlignBottom,
    AxNewMapPointIconAlignTop
};

@interface AxNewMapPointObject : AxNewMapObject

@property(nonatomic, strong, readonly) UIImage *image;
@property(nonatomic, readonly) CGPoint alignPoint;
@property(nonatomic, readonly) BOOL isDragable;

+ (instancetype)mapPointWithName:(NSString *)name
                         address:(NSString *)address
                      attributes:(NSDictionary *)attributes
                      coordinate:(CLLocationCoordinate2D)coordinate
                            icon:(UIImage *)iconImage
                       iconAlign:(AxNewMapPointIconAlign)iconAlign
                        dragable:(BOOL)dragable;

- (void)updateWithImage:(UIImage *)image;

- (void)updateWithCoordinate:(CLLocationCoordinate2D)coordinate;

@end
