//
//  AxNewMapDelegate.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 18.02.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

@import Foundation;

@class AxNewMap;
@class AxNewMapObject;

@protocol AxNewMapDelegate <NSObject>
@optional

- (void)mapDidLoad:(AxNewMap *)map;

- (void)map:(AxNewMap *)map didSelectMapObject:(AxNewMapObject *)mapObject;

- (void)map:(AxNewMap *)map didDeselectMapObject:(AxNewMapObject *)mapObject;

- (void)mapDidEndZooming:(AxNewMap *)map withState:(AxNewMapZoomingState)zoomingState;

- (void)mapDidEndMoving:(AxNewMap *)map;

- (void)singleTapOnMap:(AxNewMap *)map at:(CLLocationCoordinate2D)coordinate;

- (void)longPressOnMap:(AxNewMap *)map at:(CLLocationCoordinate2D)coordinate;

- (void)singleTapOnMapObject:(AxNewMapObject *)mapObject onMap:(AxNewMap *)map at:(CLLocationCoordinate2D)coordinate;

- (void)longPressOnMapObject:(AxNewMapObject *)mapObject onMap:(AxNewMap *)map;

- (void)singleTapOnCalloutForMapObject:(AxNewMapObject *)mapObject;

- (void)singleTapOnCalloutRightAccessoryViewForMapObject:(AxNewMapObject *)mapObject;

- (void)userDidEndMovingMapObject:(AxNewMapObject *)mapObject onMap:(AxNewMap *)map;

@end
