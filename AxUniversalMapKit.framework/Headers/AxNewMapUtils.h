//
//  AxNewMapUtils.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 25.02.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

@import UIKit;
@import Foundation;
@import CoreLocation;

@interface AxNewMapUtils : NSObject

+ (UIImage *)pinImageWithText:(NSString *)text;

+ (CLLocationCoordinate2D)middlePointForLocations:(NSArray *)locationsArray;

@end
