//
//  CategoryItem+Utilites.h
//  MoscowAtlas
//
//  Created by Sergey Starukhin on 28.01.14.
//  Copyright (c) 2014 Altarix. All rights reserved.
//

#import <AxUniversalMapKit/AxEmpFilterDataItem.h>
#import <AxUniversalMapKit/AxMap.h>

@interface AxEmpFilterDataItem (Utilites) <AxMapFilter>

+ (NSString*) entityName;

+ (instancetype)categoryItemWithName:(NSString *)name inManagedObjectContext:(NSManagedObjectContext *)context;

+ (instancetype)categoryItemWithSSId:(NSString *)ssid inManagedObjectContext:(NSManagedObjectContext *)context;

+ (instancetype)rootCtaegoryItemInManagedObjectContext:(NSManagedObjectContext *)context;

+ (NSDate *)dateOfLatestUpdateInManagedObjectContext:(NSManagedObjectContext *)context;

- (void)updateWithData:(NSDictionary *)data;

- (NSArray *)subitems;

- (NSUInteger)activeSubitemsCount;

- (NSURL *)pinIconURL;

- (AxEmpFilterDataItem *)parentItem;

@end
