//
//  AxNewMapGeometryPoint.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 04.03.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "AxNewMapAbstractGeometry.h"

@interface AxNewMapGeometryPoint : AxNewMapAbstractGeometry

@property(nonatomic, assign) CLLocationCoordinate2D coordinate;

+ (instancetype)geometryPointWithCoordinate:(CLLocationCoordinate2D)pointCoordinate;

@end
