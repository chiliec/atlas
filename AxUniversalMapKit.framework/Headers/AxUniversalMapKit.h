//
//  AxUniversalMapKit.h
//  AxUniversalMapKit
//
//  Created by Sergey Starukhin on 23.01.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AxUniversalMapKit.
FOUNDATION_EXPORT double AxUniversalMapKitVersionNumber;

//! Project version string for AxUniversalMapKit.
FOUNDATION_EXPORT const unsigned char AxUniversalMapKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AxUniversalMapKit/PublicHeader.h>


#import <AxUniversalMapKit/AxGeocoder.h>
#import <AxUniversalMapKit/AxLocationManager.h>
#import <AxUniversalMapKit/AxLocationRect.h>
#import <AxUniversalMapKit/AxMap+ArcgisGeometry.h>
#import <AxUniversalMapKit/AxMap.h>
#import <AxUniversalMapKit/AxMapCalloutView.h>
#import <AxUniversalMapKit/AxMapFilterRowData.h>
#import <AxUniversalMapKit/AxMapFilterSectionData.h>
#import <AxUniversalMapKit/AxMapFilterSource.h>
#import <AxUniversalMapKit/AxMapObject.h>
#import <AxUniversalMapKit/AxPointMapObject.h>
#import <AxUniversalMapKit/AxPolygonMapObject.h>
#import <AxUniversalMapKit/AxWebTiledLayer.h>
#import <AxUniversalMapKit/AxArcgisConfiguration.h>
#import <AxUniversalMapKit/AxArcgisGeocoder.h>
#import <AxUniversalMapKit/AxArcgisMap.h>
#import <AxUniversalMapKit/AxArcgisMapFilterSource.h>
#import <AxUniversalMapKit/AxDigiMapRoute.h>
#import <AxUniversalMapKit/AxOSMGeocoder.h>
#import <AxUniversalMapKit/AxYandexGeocoder.h>

#pragma mark - AxNewMap Headers

#import <AxUniversalMapKit/AxNewMap.h>
#import <AxUniversalMapKit/AxNewMapDelegate.h>
#import <AxUniversalMapKit/AxNewMapProtocol.h>
#import <AxUniversalMapKit/AxNewMapObject.h>
#import <AxUniversalMapKit/AxNewAbstractMapShapeObject.h>
#import <AxUniversalMapKit/AxNewMapPointObject.h>
#import <AxUniversalMapKit/AxNewMapMultiPointObject.h>
#import <AxUniversalMapKit/AxNewMapLineObject.h>
#import <AxUniversalMapKit/AxNewMapMultilineObject.h>
#import <AxUniversalMapKit/AxNewMapPolygonObject.h>
#import <AxUniversalMapKit/AxNewMapMultiPolygonObject.h>
#import <AxUniversalMapKit/AxNewGlobalConstants.h>
#import <AxUniversalMapKit/AxNewMapUtils.h>
#import <AxUniversalMapKit/AxNewMapLocationRect.h>
#import <AxUniversalMapKit/AxNewMapMath.h>
#import <AxUniversalMapKit/AxMapKitMap.h>
#import <AxUniversalMapKit/AxMapKitConfiguration.h>
#import <AxUniversalMapKit/AxNewMapClusterObject.h>
#import <AxUniversalMapKit/AxNewMapRoute.h>
//#import <AxUniversalMapKit/AxMapKitTileSource.h>
//#import <AxUniversalMapKit/AxNewCalloutView.h>

#import <AxUniversalMapKit/AxNewMapGeometry.h>
#import <AxUniversalMapKit/AxNewMapGeometryMultiPoint.h>
#import <AxUniversalMapKit/AxNewMapGeometryPolygon.h>
#import <AxUniversalMapKit/AxNewMapGeometryMultiline.h>
#import <AxUniversalMapKit/AxNewMapGeometryLine.h>
#import <AxUniversalMapKit/AxNewMapGeometryMultiPolygon.h>
#import <AxUniversalMapKit/AxNewMapGeometryPoint.h>
#import <AxUniversalMapKit/AxNewMapAbstractGeometry.h>

// AxEmpMap - перенесено из AxVCL
#import <AxUniversalMapKit/AxDynamicMapController.h>
#import <AxUniversalMapKit/AxDynamicMapFilterSource.h>
#import <AxUniversalMapKit/AxEmpBaseMapController.h>
#import <AxUniversalMapKit/AxEmpFilterDataItem+Utilites.h>
#import <AxUniversalMapKit/AxEmpFilterDataItem.h>
#import <AxUniversalMapKit/AxEmpFilterCell.h>
#import <AxUniversalMapKit/AxEmpFiltersListController.h>
#import <AxUniversalMapKit/AxEmpMap.h>

//#import <AxUniversalMapKit/>
