//
//  AxMapKitConfiguration.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 10.02.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AxUniversalMapKit/AxMap.h>
#import <AxUniversalMapKit/AxNewGlobalConstants.h>

@interface AxMapKitConfiguration : NSObject

+ (instancetype) sharedInstance;

#pragma mark - Get License

- (NSString *) licenseKey;

#pragma mark - Get URLs

- (NSString *) URLTemplateForMainTiledLayerForMode:(AxNewMapTiledLayerMode)layerMode;
- (NSString *) URLTemplateForAuxTiledLayerForMode:(AxNewMapTiledLayerMode)layerMode;
- (NSURL *) URLForRoutingService;
- (NSURL *) URLForGeocodingService;
- (NSString *) URLTemplateForTrafficJamsService;

#pragma mark - Get Zoom Offset

- (NSInteger)zoomOffsetForMainTiledLayerForMode:(AxNewMapTiledLayerMode)layerMode;
- (NSInteger)zoomOffsetForAuxTiledLayerForMode:(AxNewMapTiledLayerMode)layerMode;

#pragma mark - Update Configuration With URLs

- (void) updateURLTemplateForMainTiledLayer:(NSString *)templateString
                                    forMode:(AxNewMapTiledLayerMode)layerMode;
- (void) updateURLTemplateForAuxTiledLayer:(NSString *)templateString
                                   forMode:(AxNewMapTiledLayerMode)layerMode;

- (void) updateURLStringForRoutingService:(NSString *)urlString;
- (void) updateURLStringForGeocodingService:(NSString *)urlString;
- (void) updateURLTemplateForTrafficJamsService:(NSString *)templateString;

#pragma mark - Update Zoom Offset

- (void) updateZoomOffset:(NSInteger)zoomOffset forMainTiledLayerForMode:(AxNewMapTiledLayerMode)layerMode;
- (void) updateZoomOffset:(NSInteger)zoomOffset forAuxTiledLayerForMode:(AxNewMapTiledLayerMode)layerMode;

@end
