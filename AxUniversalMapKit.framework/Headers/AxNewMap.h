//
//  AxNewMap.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 18.02.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "AxNewMapProtocol.h"
#import "AxNewMapDelegate.h"
#import "AxNewGlobalConstants.h"
#import "AxNewCalloutView.h"

@import UIKit;

/**
 Abstract map class for subclassing, not for direct usage.
 */
@interface AxNewMap : UIView <AxNewMapProtocol, AxNewCalloutViewDelegate>

/// Delegate of the AxAbstractMap
@property (nonatomic, weak) id <AxNewMapDelegate> delegate;

@property (nonatomic, strong) UIImage *userLocationIcon;

+ (CLRegion *)moscowRegion;

- (BOOL) isCoordinateInMoscowRegion:(CLLocationCoordinate2D) coordinate;

- (void)commonInitializer;

@end
