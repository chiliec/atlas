//
//  CategoryCell.h
//  MoscowAtlas
//
//  Created by Sergey Starukhin on 30.01.14.
//  Copyright (c) 2014 Altarix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AxEmpFilterCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UIView *textView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

+ (CGFloat)heightCellInTableView:(UITableView *)tableView withTitle:(NSString *)title hasImage:(BOOL)hasImage;

+ (UIFont *)titleFont;

- (void)updateIcon:(UIImage *)icon;

@end
