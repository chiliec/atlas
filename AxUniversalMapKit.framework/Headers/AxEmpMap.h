//
//  AxEmpMap.h
//  AxEMP
//
//  Created by Sergey Starukhin on 06.03.14.
//  Copyright (c) 2014 Altarix. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NSManagedObjectContext, NSManagedObjectModel, AxEmpBaseMapController;

@interface AxEmpMap : NSObject

+ (NSBundle *)mapResourcesBundle;

+ (NSManagedObjectModel *)modelForMapObjects;

+ (NSManagedObjectModel *)addAxEmpMapModelToModel:(NSManagedObjectModel *)srcModel;

+ (AxEmpBaseMapController *)instantiateMapWithMapObjectsContext:(NSManagedObjectContext *)context;

@end
