//
//  AxNewMapObject.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 19.02.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "AxNewMapUtils.h"

@import Foundation;
@import CoreLocation;
@import UIKit;

@class AxNewMap;
@class AxNewMapLocationRect;
@class AxNewMapAbstractGeometry;

@interface AxNewMapObject : NSObject

/// Weak link to the map, created after the object was added to the map.
@property(nonatomic, weak) AxNewMap *map;
/// Linked object with the map
@property(nonatomic, weak, readonly) id object;
/// Layer id for objects classification 
@property(nonatomic, copy, readonly) NSString *layerId;
/// Object id for object details request
@property(nonatomic, copy, readonly) NSString *objectId;
/// Object name
@property(nonatomic, copy, readonly) NSString *name;
/// Object address
@property(nonatomic, copy, readonly) NSString *address;
/// Object attributes parsed from the geoJSON
@property(nonatomic, strong, readonly) NSDictionary *attributes;
/// Object geometry
@property(nonatomic, strong, readonly) AxNewMapAbstractGeometry *geometry;
/// Anchor object coordinate
@property(nonatomic, readonly) CLLocationCoordinate2D anchorCoordinate;
/// Tapped object coordinate
@property(nonatomic, readwrite) CLLocationCoordinate2D tappedCoordinate;

/** Create new instance of AxMapObjectClass according to the geometry type
*   @param geoJSON Dictionary containing parsed geoJSON data */
+ (instancetype)mapObjectWithGeoJSON:(NSDictionary *)geoJSON
                             options:(NSDictionary *)optionsDict;

/** Common initializer
*   @param geoJSON Dictionary containing parsed geoJSON data */
- (instancetype)initWithGeoJSON:(NSDictionary *)geoJSON
                        options:(NSDictionary *)optionsDict;

- (void)updateMapObjectWithDataObject:(id)dataObject;

- (void)updateWithAttributes:(NSDictionary *)attributes;

@end
