//
//  AxPointMapObject.h
//  AxUniversalMap
//
//  Created by Sergey Starukhin on 26.11.14.
//  Copyright (c) 2014 Altarix. All rights reserved.
//

#import <AxUniversalMapKit/AxMapObject.h>

@interface AxPointMapObject : AxMapObject

+ (UIImage *)defaultPinIcon;

- (void)resetPinToDefault;

/*! Обновление иконки пина. Иконка рисуется над точкой на карте, коллаут показывается от середины верхней грани иконки */
- (void)updatePinIcon:(UIImage *)pinIcon;

- (void)updatePinIcon:(UIImage *)pinIcon pinOffset:(CGPoint)pinOffset calloutOffset:(CGPoint)calloutOffset;

- (void)updateCoordinate:(CLLocationCoordinate2D)coordinate;

@end
