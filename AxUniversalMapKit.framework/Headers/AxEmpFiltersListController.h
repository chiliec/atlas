//
//  CategoryTableViewController.h
//  MoscowAtlas
//
//  Created by Sergey Starukhin on 28.01.14.
//  Copyright (c) 2014 Altarix. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AxEmpFilterDataItem;

@interface AxEmpFiltersListController : UITableViewController

- (void)configureWithCategoryItem:(AxEmpFilterDataItem *)categoryItem;

- (IBAction)resetLayersSelection;

@end

@interface AxEmpFiltersListController (Private)

- (AxEmpFilterDataItem *)filterDataItemInTableView:(UITableView *)tableView withIndexPath:(NSIndexPath *)indexPath;

- (void)configureMainTableView:(UITableView *)tableView;
- (void)configureSearchTableView:(UITableView *)tableView;

- (UITableViewCell *)createCellInTableView:(UITableView *)tableView withFilterDataItem:(AxEmpFilterDataItem *)filterItem;

- (CGFloat)heightCellInTableView:(UITableView *)tableView withFilterDataItem:(AxEmpFilterDataItem *)filterItem;

- (BOOL)shouldChangeStateFilterDataItem:(AxEmpFilterDataItem *)filterItem;

- (void)didChangedStateFilterDataItem:(AxEmpFilterDataItem *)filterItem;

@end