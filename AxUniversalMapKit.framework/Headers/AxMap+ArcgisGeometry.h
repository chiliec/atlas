//
//  AxMap+ArcgisGeometry.h
//  AxUniversalMap
//
//  Created by Sergey Starukhin on 13.05.14.
//  Copyright (c) 2014 Altarix. All rights reserved.
//

#import <AxUniversalMapKit/AxMap.h>
#import <ArcGIS/ArcGIS.h>

@interface AxMap (ArcgisGeometry)

+ (AGSPoint *) agsPointWithCoordinate:(CLLocationCoordinate2D)coordinate spatialReference:(AGSSpatialReference *)spatialReference;

+ (CLLocationCoordinate2D) coordinateFromAgsPoint: (AGSPoint*) point;

+ (AGSEnvelope *)arcGisEnvelopeFromLocationRect:(AxLocationRect *)locationRect spatialReference:(AGSSpatialReference *)spatialReference;

+ (AxLocationRect *)locationRectFromArcgisEnvelope:(AGSEnvelope *)envelope;

@end
