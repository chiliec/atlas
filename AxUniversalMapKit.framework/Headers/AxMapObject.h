//
//  AxMapObject.h
//  AxUniversalMap
//
//  Created by Sergey Starukhin on 26.11.14.
//  Copyright (c) 2014 Altarix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreLocation/CoreLocation.h>

@class AGSGraphic, AGSGeometry, UIImage, UIView, AxMap;

@interface AxMapObject : NSObject

@property (nonatomic, copy, readonly) NSString *objectID;

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *address;

@property (nonatomic, weak) AxMap *map;

/*! Эта проперть исключительно для карты, символ можно менять изнутри в подклассах,
    геометрию - нетрогать никогда, она задаётся при инициализации и может быть пересчитанакартой по своему усмотрению,
    атрибуты графика тоже лучше не торгать, а получать через проперть attributes */
@property (nonatomic, strong, readonly) AGSGraphic *graphic;

@property (nonatomic, strong, readonly) NSDictionary *attributes;

@property (nonatomic, assign, getter=isVisible) BOOL visible;

+ (instancetype)mapObjectFromDictionary:(NSDictionary *)data;

/*! значение геометрии - nil ошибкой не считается, объект просто не добавится на карту */
- (instancetype)initWithGeometry:(AGSGeometry *)geometry attributes:(NSDictionary *)attributes;

- (NSString *)calloutTitle;

- (NSString *)calloutDetail;

- (UIImage *)calloutIcon;

- (UIView *)calloutView;

- (CGPoint)calloutPixelOffset;

- (CLLocationCoordinate2D)center;

- (void)removeFromMap;

- (BOOL)isEqualMapObject:(AxMapObject *)mapObject;

@end
