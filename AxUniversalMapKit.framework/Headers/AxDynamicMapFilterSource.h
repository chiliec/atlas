//
//  AxDynamicMapFilterSource.h
//  AxVCL
//
//  Created by Sergey Starukhin on 12.03.14.
//  Copyright (c) 2014 Altarix. All rights reserved.
//

#import <AxUniversalMapKit/AxMapFilterSource.h>

@interface AxDynamicMapFilterSource : AxMapFilterSource

@end
