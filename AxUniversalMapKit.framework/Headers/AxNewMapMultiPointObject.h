//
//  AxNewMapMultiPointObject.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 19.02.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "AxNewMapObject.h"

@interface AxNewMapMultiPointObject : AxNewMapObject

@property(nonatomic, strong, readonly) UIImage *image;

- (void)updateWithImage:(UIImage *)image;

@end
