//
//  AxNewMapAbstractGeometry.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 04.03.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

@import Foundation;
@import CoreLocation;

#import "AxNewMapLocationRect.h"

@interface AxNewMapAbstractGeometry : NSObject

@property(nonatomic, assign, readonly) AxNewMapLocationRect *boundingBox;

@end
