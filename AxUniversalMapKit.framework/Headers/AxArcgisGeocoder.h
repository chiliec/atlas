//
//  AxArcgisGeocoder.h
//  AxUniversalMap
//
//  Created by Balalaev Sergey on 7/15/13.
//  Copyright (c) 2013 Altarix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AxUniversalMapKit/AxGeocoder.h>

@interface AxArcgisGeocoder : AxGeocoder

@end
