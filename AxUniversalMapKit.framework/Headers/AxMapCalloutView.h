//
//  CalloutView.h
//  eReception
//
//  Created by Sergey Starukhin on 19.03.14.
//  Copyright (c) 2014 Altarix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AxMapCalloutView : UIControl

@property (nonatomic, strong) UIImage *accessoryImage  UI_APPEARANCE_SELECTOR;

@property (nonatomic, strong) UIFont *titleFont        UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIFont *detailFont       UI_APPEARANCE_SELECTOR;

@property (nonatomic, strong) UIColor *titleColor      UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *detailColor     UI_APPEARANCE_SELECTOR;

@property (nonatomic) CGFloat maxWidth                 UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIEdgeInsets margins             UI_APPEARANCE_SELECTOR;

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *detail;

@property (nonatomic, strong) UIImage *image;

/*! Произвольный объект ассоциированный с коллаутом, внутри AxMapCalloutView никак не используется */
@property (nonatomic, strong) id attachment;

@end

@interface AxMapCalloutView (Private)

/*! Для переопределения в потомках! Если необходимо изменить количество меток на вьюхе.
 Возвращает массив UILabel'ов которые будут располагаться друг под другом.
 По умолчанию возвращает 2 метки: title и detail.
 Используется в методах - (void)sizeToFit и - (CGSize)sizeThatFits:(CGSize)size для расстановки меток */
- (NSArray *)labels;

@end