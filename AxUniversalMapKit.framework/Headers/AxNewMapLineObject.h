//
//  AxNewMapLineObject.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 19.02.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "AxNewAbstractMapShapeObject.h"

@interface AxNewMapLineObject : AxNewAbstractMapShapeObject

@property(nonatomic, strong, readonly) NSArray *points;

+ (instancetype)lineObjectWithPointsArray:(NSArray *)points
                                     name:(NSString *)name
                               attributes:(NSDictionary *)attributes
                                  options:(NSDictionary *)optionsDict;

- (void)updateLineWithLineColor:(UIColor *)alineColor
                      lineWidth:(CGFloat)alineWidth;

@end
