//
//  AxGeocoder.h
//  AxUniversalMap
//
//  Created by Balalaev Sergey on 7/15/13.
//  Copyright (c) 2013 Altarix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class AxGeocoderData;

typedef void(^AxGeocoderHandler)(NSArray * result, NSString * errorMesage, AxGeocoderData * data);

@interface AxGeocoderData : NSObject

@property (nonatomic, strong) NSString * address;
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) AxGeocoderHandler handler;

- (void) completionWithResult: (NSArray*) result;
- (void) completionWithErrorMessage: (NSString*) errorMessage;

- (void) cancel;

@end

@interface AxResultGeocoder : NSObject
{
    
}
- (id) initWithAddress: (NSString*) address coordinate: (CLLocationCoordinate2D) coordinate;

@property (nonatomic, readonly) NSString * address;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

@end

@interface AxGeocoder : NSObject
{
@protected
    NSMutableSet * allRequests;
}

- (AxGeocoderData *) startGeocodingWithAdress: (NSString*) address completionHandler: (AxGeocoderHandler) completionHandler;
- (AxGeocoderData *) startReverseGeocodingWithCoordinate: (CLLocationCoordinate2D) coordinate completionHandler: (AxGeocoderHandler) completionHandler;

- (void) allCancel;

@end