//
//  AxDynamicMapController.h
//  AxVCL
//
//  Created by Sergey Starukhin on 11.03.14.
//  Copyright (c) 2014 Altarix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface AxDynamicMapController : UIViewController

@property (nonatomic) BOOL isShowNavBar;
@property (nonatomic) BOOL scalePinIcons;
@property (nonatomic, strong) NSArray *layers;

- (void)markFrom:(CLLocationCoordinate2D)coordinate;

- (void)markFrom:(NSNumber *)arcgisx arcgisy:(NSNumber *)arcgisy;

@end
