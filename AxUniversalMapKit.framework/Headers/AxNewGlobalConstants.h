//
//  AxNewGlobalConstants.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 19.02.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

@import CoreLocation;

#ifndef AxUniversalMapKit_AxNewGlobalConstants_h
#define AxUniversalMapKit_AxNewGlobalConstants_h

typedef enum {
    AxNewMapSchemeTiledLayer = 0,
    AxNewMapHybridTiledLayer,
    AxNewMapSatelliteTiledLayer,
} AxNewMapTiledLayerMode;

typedef enum {
    AxNewMapDefaultState,
    AxNewMapReachedMaxResolution,
    AxNewMapReachedMinResolution
} AxNewMapZoomingState;

#define kAxNewMapMaxLong                                           180.0
#define kAxNewMapMaxLat                                            90.0

static const double kAxNewMapMinLatitude = -kAxNewMapMaxLat;
static const double kAxNewMapMaxLatitude = kAxNewMapMaxLat;
static const double kAxNewMapMinLongitude = -kAxNewMapMaxLong;
static const double kAxNewMapMaxLongitude = kAxNewMapMaxLong;

///Default map zooming/panning parameters
///Zoom Constraint Constants

#define kAxNewMapDefaultMaxScale                                   18.f
#define kAxNewMapDefaultMinScale                                   9.f
#define kAxNewMapDefaultZoomValue                                  12.f

///Pan Constraint Constants

#define kAxNewMapDefaultConstraintsSouthWest                       CLLocationCoordinate2DMake(54.242271314699, 35.096418260798)
#define kAxNewMapDefaultConstraintsNorthEast                       CLLocationCoordinate2DMake(56.972266458959, 40.287829585914)
#define kAxNewMapMoscowCenterCoordinates                           CLLocationCoordinate2DMake(55.7522200, 37.6155600)

///Map Objects
///Map Objects Types

#define kAxNewMapObjectTypePoint                                   @"Point"
#define kAxNewMapObjectTypeMultiPoint                              @"MultiPoint"
#define kAxNewMapObjectTypeLine                                    @"LineString"
#define kAxNewMapObjectTypeMultiLine                               @"MultiLineString"
#define kAxNewMapObjectTypePolygon                                 @"Polygon"
#define kAxNewMapObjectTypeMultiPoligon                            @"MultiPolygon"

///Map Layers

#define kAxNewMapObjectUnprovidedLayerID                           @"unprovided_layer_id"
#define kAxNewMapObjectUnprovidedObjectID                          @"unprovided_object_id"

///Map Styling

#define kAxNewMapLineColor                                         0xDF1B41
#define kAxNewMapFillColor                                         0xDF6941
#define kAxNewMapLineWidth                                         1.f

#endif
