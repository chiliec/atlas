//
//  AxNewMapGeometryPolygon.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 04.03.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "AxNewMapAbstractGeometry.h"

@interface AxNewMapGeometryPolygon : AxNewMapAbstractGeometry

@property(nonatomic, strong) NSArray *points;
@property(nonatomic, strong) NSArray *interiorPolygons;

+ (instancetype)geometryPolygonWithPoints:(NSArray *)points
                         interiorPolygons:(NSArray *)interiorPolygons;

@end
