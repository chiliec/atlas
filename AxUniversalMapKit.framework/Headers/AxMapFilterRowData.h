//
//  AxMapFilterRowData.h
//  AxUniversalMap
//
//  Created by Balalaev Sergey on 7/15/13.
//  Copyright (c) 2013 Altarix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AxUniversalMapKit/AxMap.h>

@interface AxMapFilterRowData : NSObject <AxMapFilter>

@property (nonatomic) BOOL isOn;
@property (nonatomic, copy, readonly) NSString *serverSideId;

+ (instancetype)filterRowDataServerSideId:(NSString *)ssid state:(BOOL)isOn;

@end
