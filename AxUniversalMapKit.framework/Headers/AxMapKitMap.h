//
//  AxMapKitMap.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 10.02.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import <AxUniversalMapKit/AxUniversalMapKit.h>
#import <AxUniversalMapKit/AxNewMap.h>

@class RMMapView;

@interface AxMapKitMap : AxNewMap

@property (nonatomic, strong) RMMapView *mapView;

@end
