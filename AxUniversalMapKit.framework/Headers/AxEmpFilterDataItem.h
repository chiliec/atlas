//
//  CategoryItem.h
//  MoscowAtlas
//
//  Created by Sergey Starukhin on 30.01.14.
//  Copyright (c) 2014 Altarix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface AxEmpFilterDataItem : NSManagedObject

@property (nonatomic, retain) NSNumber * curptid;
@property (nonatomic, retain) NSData * iconRawData;
@property (nonatomic, retain) NSString * iconurl;
@property (nonatomic, retain) NSNumber * islayer;
@property (nonatomic, retain) NSNumber * isleaf;
@property (nonatomic, retain) NSNumber * itemActive;
@property (nonatomic, retain) NSDate * lastUpdateTimestamp;
@property (nonatomic, retain) NSNumber * levelval;
@property (nonatomic, retain) NSNumber * parentpstid;
@property (nonatomic, retain) NSNumber * pstid;
@property (nonatomic, retain) NSString * ptname;
@property (nonatomic, retain) NSString * sodgd_ly;
@property (nonatomic, retain) NSString * pinurl;
@property (nonatomic, retain) NSNumber * needScalePinIcon;

@end
