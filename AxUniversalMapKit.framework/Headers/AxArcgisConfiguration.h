//
//  AxArcgisConfiguration.h
//  AxUniversalMap
//
//  Created by Sergey Starukhin on 25.11.13.
//  Copyright (c) 2013 Altarix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AxUniversalMapKit/AxMap.h>

@interface AxArcgisConfiguration : NSObject

+ (instancetype)sharedInstance;

+ (NSArray *)bundles;

- (NSURL *)URLForMainTiledLayerForMode:(AxMapTiledLayerMode)layerMode;
- (NSURL *)URLForAuxTiledLayerForMode:(AxMapTiledLayerMode)layerMode;

- (NSURL *)URLForRoutingService;
- (NSURL *)URLForGeocodingService;

- (UIImage *)defaultImageForPin;

- (UIImage *)defaultImageForLayerWithName:(NSString *)layerName;

- (BOOL)licenseSDK:(NSError **)error;

@end
