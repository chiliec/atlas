//
//  AxNewMapMath.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 19.03.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

@import Foundation;
@import UIKit;

@class AxNewMapObject;

@interface AxNewMapMath : NSObject

+ (CLLocationCoordinate2D)wgs84FromSphericalMercator:(NSDictionary *)mercatorDict;

+ (NSDictionary *)mercatorFromWgs84:(CLLocationCoordinate2D)coordinate;

+ (CGFloat)distanceBetweenScreenPoint:(CGPoint)tapPoint
                         andMapObject:(AxNewMapObject *)mapObject;

@end
