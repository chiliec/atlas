//
//  AxNewAbstractMapShapeObject.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 19.02.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "AxNewMapObject.h"

/** Abstract subclass for creating shapes. You shouldn't create the instances of this class directly. Instead, you should create instances of the AxNewMapPolylineObject or AxNewMapPolygonObject classes. */
@interface AxNewAbstractMapShapeObject : AxNewMapObject
/// Line color for shape bounds
@property(nonatomic, strong, readonly) UIColor *lineColor;
/// Line width for shape bounds
@property(nonatomic, assign, readonly) CGFloat lineWidth;
/// Fill color for the internal area if the shape
@property(nonatomic, strong, readonly) UIColor *fillColor;

@end