//
//  AxNewMapGeometryMultiline.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 05.03.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "AxNewMapAbstractGeometry.h"

@interface AxNewMapGeometryMultiline : AxNewMapAbstractGeometry

@property(nonatomic, strong) NSArray *lines;

+ (instancetype)geometryMultilineWithLines:(NSArray *)lines;

@end
