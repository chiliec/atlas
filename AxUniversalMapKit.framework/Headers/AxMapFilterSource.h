//
//  AxMapFilterSource.h
//  AxUniversalMap
//
//  Created by Balalaev Sergey on 7/15/13.
//  Copyright (c) 2013 Altarix. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const kFilterSourceErrorDomain;

FOUNDATION_EXPORT NSString *const kFilterSourceAllFiltersToken;

typedef enum {
    FilterSourceErrorUnknown = 1,
    FilterSourceErrorServerReturnNoData,
    FilterSourceNetworkError
} FilterSourceErrorCode;

@class AxMapFilterSource;

@interface AxMapFilterSource : NSObject

@property (nonatomic, readonly) NSError *error;

- (BOOL)filtersDataLoaded;

- (NSOperation *)loadDataOperation;

- (NSArray *)layersNames;

- (NSString *)nameForLayerWithId:(NSInteger)layerId;
- (NSInteger)idForLayerWithName:(NSString *)name;

// это метод для карты - он вернёт как минимум AGSGraphicsLayer (может вернуть AGSFeaturesLayer)
- (id)newLayerWithName:(NSString *)name;

@end
