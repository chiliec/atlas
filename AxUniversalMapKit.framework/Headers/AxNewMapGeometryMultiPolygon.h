//
//  AxNewMapGeometryMultiPolygon.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 04.03.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "AxNewMapAbstractGeometry.h"

@interface AxNewMapGeometryMultiPolygon : AxNewMapAbstractGeometry

@property(nonatomic, strong) NSArray *polygons;

+ (instancetype)geometryMultiPolygonWithPolygons:(NSArray *)polygons;

@end
