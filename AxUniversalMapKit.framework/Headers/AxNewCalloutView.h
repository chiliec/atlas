//
//  AxNewCalloutView.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 13.03.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

@class AxNewMapObject;
@class AxNewMapPointObject;
@class AxNewMap;
@class SMCalloutView;

@protocol AxNewCalloutViewDelegate <NSObject>
@optional

- (void)didClickOnCalloutForMapObject:(AxNewMapObject *) mapObject;
- (void)didClickOnCalloutRightAccessoryViewForMapObject:(AxNewMapObject *) mapObject;

@end

@import CoreLocation;
@import CoreGraphics;

#import <UIKit/UIKit.h>

@interface AxNewCalloutView : NSObject

@property(nonatomic, strong, readonly) AxNewMapObject *associatedMapObj;
@property(nonatomic, strong, readonly) AxNewMapPointObject *anchorPointObject;
@property(nonatomic, strong, readonly) AxNewMap *targetMap;

@property(nonatomic, strong) SMCalloutView *internalCallout;

+ (AxNewCalloutView *)calloutForMapObject:(AxNewMapObject *)mapObject
                                    onMap:(AxNewMap *)map
                                  inPoint:(CGPoint)point
                                  options:(NSDictionary *)options;

- (void)show;

- (void)dismiss;

@end
