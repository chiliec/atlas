//
//  AxDigiMapRoute.h
//  AxUniversalMap
//
//  Created by Alexey Kochetov on 10.11.14.
//  Copyright (c) 2014 Altarix. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AxMapRoute;
@class AGSSpatialReference;

typedef NS_ENUM(NSInteger, AxDigiMapRouteType) // Вид транспорта для расчета маршрута
{
    AxDigiMapRouteTypeAuto,         // Автомобильный
    AxDigiMapRouteTypePedestrian,   // Пешеходный
    AxDigiMapRouteTypeTransport     // Общественный транспорт
};

typedef NS_ENUM(NSInteger, AxDigiMapOptimizeType) // Вид оптимизации
{
    AxDigiMapOptimizeTypeDistance,  // По расстоянию
    AxDigiMapOptimizeTypeTime       // По времени
};

typedef NS_ENUM(NSInteger, AxDigiMapLanguageType) // Локализация строк в ответе
{
    AxDigiMapLanguageTypeRU,        // Русский язык
    AxDigiMapLanguageTypeEN         // Английский язык
};

typedef NS_ENUM(NSInteger, AxDigiMapSpeedCalculationType) // Расчет скорости движения
{
    AxDigiMapSpeedCalculationTypeIdeal,         // Идеальный вариант
    AxDigiMapSpeedCalculationTypeOnline,        // В режиме реального времени с учетом пробок
    AxDigiMapSpeedCalculationTypeHistorySimple, // Исторические скорости на конкретный момент времени
    AxDigiMapSpeedCalculationTypeHistoryTrace   // Исторические скорости на конкретный момент времени с учетом времени на движение по маршруту
};

@interface AxDigiMapRoute : NSObject <AxMapRoute>

+ (instancetype) digiMapRouteWithPoints:(NSArray *)points
                               language:(AxDigiMapLanguageType)langType
                              routeType:(AxDigiMapRouteType)routeType
                             optimizeBy:(AxDigiMapOptimizeType)optimizeType
                       speedCalculation:(AxDigiMapSpeedCalculationType)speedCalcType
                            useBadRoads:(BOOL)useBadRoads
                       spatialReference:(AGSSpatialReference *)spatialReference;

@end
