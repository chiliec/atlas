//
//  AxNewMapPolygonObject.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 19.02.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "AxNewAbstractMapShapeObject.h"

@interface AxNewMapPolygonObject : AxNewAbstractMapShapeObject

- (void)updatePolygonWithFillColor:(UIColor *)polFillColor
                         lineColor:(UIColor *)polLineColor
                         lineWidth:(CGFloat)polLineWidth;

@end
