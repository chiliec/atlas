//
//  AxMapFilterSectionData.h
//  AxUniversalMap
//
//  Created by Balalaev Sergey on 7/15/13.
//  Copyright (c) 2013 Altarix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AxUniversalMapKit/AxMapFilterRowData.h>

@interface AxMapFilterSectionData : NSObject <NSFastEnumeration> {
    NSArray *rowsData;
}

@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic) BOOL isExpanded;

- (BOOL)hasChanges;

- (NSInteger)rowsCount;
- (NSInteger)selectedRowsCount;

- (AxMapFilterRowData *)rowAtIndex:(NSUInteger)index;

- (void)deselectAllFilters;

- (void)expand;
- (void)collapse;

+ (instancetype)filterSectionWithName:(NSString *)name rowsArray:(NSArray *)rows;

@end
