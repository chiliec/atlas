//
//  AxArcgisMap.h
//  AxUniversalMap
//
//  Created by Balalaev Sergey on 7/15/13.
//  Copyright (c) 2013 Altarix. All rights reserved.
//

#import <AxUniversalMapKit/AxMap+ArcgisGeometry.h>
#import <ArcGIS/ArcGIS.h>

@class AGSPoint, AGSSpatialReference;

@interface AxArcgisMap : AxMap

@property (nonatomic, strong) AGSMapView *mapView;

//! @private
@property (nonatomic, strong) AGSEnvelope *currentAppealsEnvelope;
@property (nonatomic, strong) AxLocationRect * lastRect;

@end
