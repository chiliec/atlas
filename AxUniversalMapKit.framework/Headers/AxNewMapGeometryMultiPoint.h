//
//  AxNewMapGeometryMultiPoint.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 05.03.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "AxNewMapAbstractGeometry.h"

@interface AxNewMapGeometryMultiPoint : AxNewMapAbstractGeometry

@property(nonatomic, strong) NSArray *points;

+ (instancetype)geometryMultiPointWithPoints:(NSArray *)points;

@end
