//
//  AxNewMapGeometry.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 04.03.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#ifndef AxUniversalMapKit_AxNewMapGeometry_h
#define AxUniversalMapKit_AxNewMapGeometry_h

#import "AxNewMapAbstractGeometry.h"
#import "AxNewMapGeometryPoint.h"
#import "AxNewMapGeometryMultiPoint.h"
#import "AxNewMapGeometryLine.h"
#import "AxNewMapGeometryMultiline.h"
#import "AxNewMapGeometryPolygon.h"
#import "AxNewMapGeometryMultiPolygon.h"

#endif
