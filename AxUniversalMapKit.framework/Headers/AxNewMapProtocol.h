//
//  AxNewMapProtocol.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 19.02.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "AxNewGlobalConstants.h"

@import Foundation;
@import CoreLocation;
@import UIKit;

@class AxNewMapObject;
@class AxNewMapLocationRect;
@class AxNewMapPointObject;

/** The AxNewMapProtocol describes the minimum behaviour and properties for the AxNewMap views. */
@protocol AxNewMapProtocol <NSObject>

@required

/** @name Configuring the Supported Zoom Levels. */

/// Current zoom value
@property(nonatomic, assign) float zoom;
/// The minimum zoom supported by the map
@property(nonatomic, assign) float minZoom;
/// The maximum zoom supported by the map
@property(nonatomic, assign) float maxZoom;
/// Map constraints
@property(nonatomic, strong) AxNewMapLocationRect *constraintsRect;

#pragma mark -

/** @name Stuff to Update Tiled Layers */

@property(nonatomic, readonly) AxNewMapTiledLayerMode tiledLayerMode;

- (void)updateTiledLayer:(AxNewMapTiledLayerMode)layerMode;

/** @name Map Layers Above The Substrate */

- (void)addTiledLayerWithURLTemplate:(NSString *)urlTemplate
                          uniqueName:(NSString *)uniqueName
                      zoomCorrection:(double)zoomCorrection;

- (void)removeTiledLayerWithUniqueName:(NSString *)uniqueName;

- (id)tiledLayerOnMapWithName:(NSString *)uniqueName; // Результат специфичен для реализации

/** @name Methods to Manage Map Objects. */

- (NSSet *)objectsOnMap;

- (void)addMapObject:(AxNewMapObject *)mapObject;

- (void)addMapObjects:(id <NSFastEnumeration>)objects;

- (void)removeMapObject:(AxNewMapObject *)mapObject;

- (void)removeMapObjects:(id <NSFastEnumeration>)objects;

- (void)removeAllMapObjects;

/** @name Callouts. */

- (void)displayCalloutForMapObject:(AxNewMapObject *)mapObject
                      inCoordinate:(CLLocationCoordinate2D)coordinate
                           options:(NSDictionary *)options;

- (void)displayCalloutForMapObject:(AxNewMapObject *)mapObject
                           options:(NSDictionary *)options;

- (void)dismissCallout;

/** The center coordinate of the map view. */
@property (nonatomic, assign) CLLocationCoordinate2D centerCoordinate;

/** Set the map center to a given coordinate.
 *   @param coordinate A coordinate to set as the map center.
 *   @param animated Whether to animate the change to the map center. */
- (void)setCenterCoordinate:(CLLocationCoordinate2D)coordinate animated:(BOOL)animated;

/** @name Methods to Control Zoom of the Map. */

/** Zoom the map to 2x scale if possible */
- (void)zoomIn;

/** Zoom the map to 0.5x scale if possible */
- (void)zoomOut;

/** Zoom to coordinate with zoom value
*   @param coordinate The target coordinate to zoom in WGS84 coordinate system.
*   @param zoomValue Zoom value to be applied to the map in target point. */
- (void)zoomToCoordinate:(CLLocationCoordinate2D)coordinate withZoomValue:(float)zoomValue;

- (void)zoomToCoordinate:(CLLocationCoordinate2D)coordinate withZoomValue:(float)zoomValue animated:(BOOL)animated;

- (void)zoomToCoordinate:(CLLocationCoordinate2D)coordinate withZoomFactor:(CGFloat)factor animated:(BOOL)animated;

- (void)zoomToBoundsRect:(AxNewMapLocationRect *)boundsRect;

/** @name Map bounds */

- (AxNewMapLocationRect *)mapBounds;

/** @name User Location */

- (CLLocationCoordinate2D)userLocationCoordinates;

/** @name Converting Coordinates */

- (CGPoint)coordinateToPixel:(CLLocationCoordinate2D)coordinate;

- (CLLocationCoordinate2D)pixelToCoordinate:(CGPoint)pixelCoordinate;

@end
