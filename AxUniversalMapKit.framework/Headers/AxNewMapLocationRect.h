//
//  AxNewMapLocationRect.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 03.03.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

@import Foundation;
@import CoreLocation;

@interface AxNewMapLocationRect : NSObject

@property(nonatomic, readonly) double minLongitude;
@property(nonatomic, readonly) double minLatitude;
@property(nonatomic, readonly) double maxLongitude;
@property(nonatomic, readonly) double maxLatitude;

@property(nonatomic, readonly) CLLocationCoordinate2D southWest;
@property(nonatomic, readonly) CLLocationCoordinate2D northEast;

+ (instancetype)locationRectangleWithPoint1:(CLLocationCoordinate2D)point1
                                     point2:(CLLocationCoordinate2D)point2;

+ (instancetype)locationRectangleWithPoints:(NSArray *)points;

- (BOOL)isEqualToRect:(AxNewMapLocationRect *)rect;

- (BOOL)isSmallerThanRect:(AxNewMapLocationRect *)rect;

- (CLLocationCoordinate2D)centerPoint;

- (void)changeRectWithFactor:(float)factor;

@end
