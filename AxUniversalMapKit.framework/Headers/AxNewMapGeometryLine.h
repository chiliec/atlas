//
//  AxNewMapGeometryLine.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 04.03.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "AxNewMapAbstractGeometry.h"

@interface AxNewMapGeometryLine : AxNewMapAbstractGeometry

@property(nonatomic, strong) NSArray *points;

+ (instancetype)geometryLineWithPoints:(NSArray *)points;

@end
