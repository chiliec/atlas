//
//  AxNewMapRoute.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 24.03.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

@import Foundation;

@class AxNewMapLineObject;

typedef NS_ENUM(NSInteger, AxNewMapRouteState) {
    AxNewMapRouteNotStarted,
    AxNewMapRouteSolving,
    AxNewMapRouteSolved,
    AxNewMapRouteStateFailed
};

@interface AxNewMapRoute : NSObject

@property (nonatomic, readonly) double time;
@property (nonatomic, readonly) double length;
@property (nonatomic, strong, readonly) NSError *error;
@property (nonatomic, strong, readonly) AxNewMapLineObject *geometryObject;
@property (nonatomic, readonly) AxNewMapRouteState state;

- (AxNewMapRouteState) state;
- (double) length;
- (double) time;
- (NSError *) error;

- (AxNewMapLineObject *) geometryObject;

- (void)solveWithHandler:(dispatch_block_t)handler;

@end
