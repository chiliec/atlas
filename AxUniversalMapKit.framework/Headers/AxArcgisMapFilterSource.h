//
//  AxArcgisMapFilterSource.h
//  AxUniversalMap
//
//  Created by Sergey Starukhin on 03.03.14.
//  Copyright (c) 2014 Altarix. All rights reserved.
//

#import <AxUniversalMapKit/AxMapFilterSource.h>

@class AGSGraphicsLayer;

@interface AxArcgisMapFilterSource : AxMapFilterSource

- (id)initWithServiceURL:(NSURL *)serviceURL;

- (AGSGraphicsLayer *)newLayerWithName:(NSString *)serverSideId;

@end
