//
//  AxMapKitTileSource.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 12.02.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "RMAbstractWebMapSource.h"

@interface AxMapKitTileSource : RMAbstractWebMapSource

@property (nonatomic) NSString *tileCacheKey;

- (id) initWithURLTemplate:(NSString *)URLTemplate
        withZoomCorrection:(float)zoomCorrection
              tileCacheKey:(NSString *)tileCacheKey;

@end
