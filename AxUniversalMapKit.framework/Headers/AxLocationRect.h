//
//  AxLocationRect.h
//  AxUniversalMap
//
//  Created by Balalaev Sergey on 3/6/14.
//  Copyright (c) 2014 Altarix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface AxLocationRect : NSObject <NSCoding>

@property (nonatomic) double minLongitude;
@property (nonatomic) double minLatitude;
@property (nonatomic) double maxLongitude;
@property (nonatomic) double maxLatitude;

+ (id) locationRectangleWithPoint1: (CLLocationCoordinate2D) point1 point2: (CLLocationCoordinate2D) point2;

+ (id) locationRectangleWithMinLongitude: (double) minLongitude minLatitude: (double) minLatitude maxLongitude: (double) maxLongitude maxLatitude: (double) maxLatitude;

- (BOOL)isEqualToRect: (AxLocationRect*) rect;

- (CLLocationCoordinate2D)center;

@end
