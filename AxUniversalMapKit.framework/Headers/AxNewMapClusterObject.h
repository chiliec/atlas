//
//  AxNewMapClusterObject.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 24.03.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import <AxUniversalMapKit/AxUniversalMapKit.h>

@interface AxNewMapClusterObject : AxNewMapObject

@property(nonatomic, strong, readonly) UIImage *image;
@property(nonatomic, strong, readonly) AxNewMapLocationRect *clusterExtent;
@property(nonatomic, assign, readonly) double clusterObjectsCount;
@property(nonatomic, strong, readonly) NSArray *clusterObjects;
@property(nonatomic, readonly) CGPoint alignPoint;

- (instancetype)initWithGeoJSON:(NSDictionary *)geoJSON
                        options:(NSDictionary *)optionsDict;

- (void)updateWithImage:(UIImage *)newImage;

@end
