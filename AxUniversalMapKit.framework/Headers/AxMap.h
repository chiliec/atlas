//
//  AxMap.h
//  AxUniversalMap
//
//  Created by Balalaev Sergey on 7/15/13.
//  Copyright (c) 2013 Altarix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AxUniversalMapKit/AxMapFilterSource.h>
#import <AxUniversalMapKit/AxLocationRect.h>
#import <AxUniversalMapKit/AxMapObject.h>
#import <AxUniversalMapKit/AxDigiMapRoute.h>

typedef enum {
    AxMapSchemeTiledLayer = 0,
    AxMapHybridTyledLayer,
    AxMapSatelliteTiledLayer,
} AxMapTiledLayerMode;

typedef enum {
    AxMapDefaultState,
    AxMapReachedMaxResolution,
    AxMapReachedMinResolution
} AxMapZoomingState;

typedef enum {
    AxMapGPSAutoPanModeOff = 0,          /*!< The map does not auto pan, and the location symbol may move off screen.*/
    AxMapGPSAutoPanModeDefault,          /*!< The map recenters on the location symbol whenever it moves out of the wander extent*/
    AxMapGPSAutoPanModeNavigation,       /*!< The location symbol is fixed and always points to the top edge of the device. The map pans and rotates based on location and course updates. */
	AxMapGPSAutoPanModeCompassNavigation /*!< The location symbol is fixed and always points to the top edge of the device. The map pans and rotates based on location and heading (magnetic) updates. */
} AxMapGPSAutoPanMode;

// выравнивание изображения графика относительно географической точки, по умолчанию - AxMapSymbolAlignmentCenter середина символа совпадает с точкой
typedef enum {
    AxMapSymbolAlignmentCenter,
    AxMapSymbolAlignmentTop,
    AxMapSymbolAlignmentDown,
    AxMapSymbolAlignmentLeft,
    AxMapSymbolAlignmentRight
} AxMapSymbolAlignment;

typedef NS_ENUM(NSInteger, AxMapCalloutPosition) {
    AxMapCalloutPositionAny,
    AxMapCalloutPositionTop,
    AxMapCalloutPositionBottom,
    AxMapCalloutPositionLeft,
    AxMapCalloutPositionRight
};

@class AxMap, AGSDirectionGraphic, AGSGraphic, AxWebTiledLayer;

@protocol AxMapPoint <NSObject>

@property (nonatomic, readonly) CLLocationCoordinate2D pointCoordinate;
@property (nonatomic, readonly, copy) NSString *pointTitle;
@property (nonatomic, readonly, copy) NSString *pointSubtitle;
@property (nonatomic, readonly, retain) NSDictionary *pointAttributes;

@optional

/*! метод реализован в объектах которые возвращаются картой делегату при тапе на графики из фьючерсных слоёв, внутри карты - не используется */
- (NSString *)localizedAttributeName:(NSString *)attrName;

// методы имеют приоритет над методами делегата и настройками карты

- (UIImage *)iconImage;

- (CGPoint)iconOffset; // абсолютное значение смещения имеет приоритет над iconAlignment

- (AxMapSymbolAlignment)iconAlignment;

@end

@protocol AxMapRoute <NSObject>

- (NSArray *)sourcePoints;
- (NSUInteger)stepCount;
- (NSString *)descriptionOfStep:(NSUInteger)stepIndex;

- (BOOL)isSolvingInProgress;
- (BOOL)isSolved;
- (void)solveWithHandler:(dispatch_block_t)handler;
- (NSError *)error;

- (double)totalLength;
- (double)totalTime;

- (AGSDirectionGraphic *) directionGraphicOfStep: (NSUInteger) stepIndex;
- (AGSGraphic *) routeGraphic;

@end

@protocol AxMapFilter <NSObject>

- (BOOL)filterIsOn;
- (NSString *)serverSideId;

@end

@protocol AxMapDelegate <NSObject>

@optional

/*! метод будет вызван если у точки нет метода iconImage */
- (UIImage *)map:(AxMap *)map imageForPoint:(id <AxMapPoint>)point;

/*! Кастомная вьюха для коллаута, если вернётся не nil - тапы по коллауту обрабатывать самостоятельно!!!
    Если метод вернёт nil - карта создаст AxMapCalloutView и в случае тапа попытается вызвать метод - (void)map:(AxMap *)map didClickAtCalloutView:(AxMapCalloutView *)calloutView */
- (UIView *)map:(AxMap *)map customCalloutViewForPoint:(id <AxMapPoint>)point;

/*! Метод будет вызван в случае тапа по коллауту созданному картой!
    Т.е. если метод делегата - (UIView *)map:(AxMap *)map customCalloutViewForPoint:(id <AxMapPoint>)point не определён или вернул nil.
    Возвращаемое значение - автоскрытие коллаута */
- (BOOL)map:(AxMap *)map didClickAtCalloutForPoint:(id <AxMapPoint>)point;
- (BOOL)map:(AxMap *)map shouldShowCalloutForPoint:(id <AxMapPoint>)point;
- (void)mapDidDismissCallout:(AxMap *)map;
- (void)mapDidLoad: (AxMap *)map;
- (void)map:(AxMap *)map didEndZoomingWithState:(AxMapZoomingState) state;
- (void)mapDidEndPanning:(AxMap *)map;
- (void)map:(AxMap *)map didChangeToLocationRect: (AxLocationRect*) locationRect;
- (void)map:(AxMap *)map userCoordinateDidChangeTo:(CLLocationCoordinate2D)coordinate;
- (void)map:(AxMap *)map didClickAtPoint:(id <AxMapPoint>)point;
- (void)map:(AxMap *)map didClickAtCoordinate:(CLLocationCoordinate2D)coordinate;
- (void)map:(AxMap *)map didTapAndHoldAtPoint:(id <AxMapPoint>)point;
- (void)map:(AxMap *)map didTapAndHoldAtCoordinate:(CLLocationCoordinate2D)coordinate;

// иконка для всех объектов слоя
- (UIImage *)map:(AxMap *)map imageForLayerWithName:(NSString *)layerName;

/*
- (CGPoint)map:(AxMap *)map iconOffsetForLayerWithName:(NSString *)layerName;
- (AxMapSymbolAlignment)map:(AxMap *)map algnmentForLayerWithName:(NSString *)layerName;
*/
/*! Кастомная вьюха для коллаута, если вернётся не nil - тапы по коллауту обрабатывать самостоятельно!!!
    Если метод вернёт nil - карта вызовет метод [AxMapObject calloutView] который в общем случае 
    создаст AxMapCalloutView и в случае тапа попытается вызвать метод - (void)map:(AxMap *)map didClickAtCalloutView:(AxMapCalloutView *)calloutView */
- (UIView *)map:(AxMap *)map customCalloutViewForMapObject:(AxMapObject *)mapObj;
- (BOOL)map:(AxMap *)map shouldShowCalloutForMapObject:(AxMapObject *)mapObj;
- (BOOL)map:(AxMap *)map didClickAtCalloutForMapObject:(AxMapObject *)mapObj;
- (void)map:(AxMap *)map didClickAtCoordinate:(CLLocationCoordinate2D)coordinate mapObject:(AxMapObject *)mapObject;
- (void)map:(AxMap *)map didTapAndHoldAtCoordinate:(CLLocationCoordinate2D)coordinate mapObject:(AxMapObject *)mapObject;

@end

@interface AxMap : UIView

@property (nonatomic, weak) id <AxMapDelegate> delegate;
@property (nonatomic, readonly) AxMapTiledLayerMode tiledLayerMode;
@property (nonatomic, strong) AxMapFilterSource *filterSource;
@property (nonatomic) AxMapSymbolAlignment symbolAlignment;

/*! Инсеты для компенсации прозрачных панелек в iOS 7.0. По дефолту - нули. Пока не реализовано. */
@property (nonatomic) UIEdgeInsets contentAreaInsets;

- (void)zoomInMap;
- (void)zoomOutMap;
- (void)zoomToCoordinate:(CLLocationCoordinate2D) coordinate withZoomFactor:(CGFloat)factor;
- (void)zoomToCoordinate:(CLLocationCoordinate2D) coordinate withZoomFactor:(CGFloat)factor centerOffset:(CGPoint)offset animated:(BOOL)animated;
- (void)updateTiledLayer:(AxMapTiledLayerMode)layerMode;
- (void)updateFilterLayersFromArray:(NSArray *)filters;
- (void)updateFilterLayersForFilterRow:(id <AxMapFilter>)row;
- (void)clearFilterLayers;
- (void)reloadFilterLayerWithName:(NSString *)layerName;

/*Позволяет добавить на карту не аркгисовские плиточные карточные слои*/
- (void)addWebTiledLayer: (AxWebTiledLayer *)layer;
- (void)removeWebTiledLayer: (AxWebTiledLayer *) layer;
- (void)removeTiledLayerWithName: (NSString *) layerName;
- (BOOL)isTiledLayerOnMapWithName: (NSString *) layerName;

/*! возвращает имя фьючерсного слоя слоя для объектов находящихся во фьючерсных слоях, для пользовательских точек вернётся nil */
- (NSString *)layerNameForPoint:(id <AxMapPoint>)point;

/*! Устанавливает цвет коллаута карты (не AxMapCalloutView и не кастомного коллаута).
    color - нижняя половина коллаута, highlightColor - верхняя */
- (void)setCalloutColor:(UIColor *)color highlightColor:(UIColor *)highlightColor;
- (void)setCalloutMargin:(CGSize)size cornerRadius:(CGFloat)cornerRadius;
- (void)setCalloutPosition:(AxMapCalloutPosition)position;

/*! Устанавливает цвет текста на дефолтном коллауте (AxMapCalloutView) через appearance - лучше не использовать! */
- (void)setCalloutSubtitleColor:(UIColor *)subtitleColor titleColor:(UIColor *)titleColor NS_DEPRECATED_IOS(4_0, 5_0);
- (void)hideCallout;
- (void)showCalloutForPoint:(id <AxMapPoint>)point animated:(BOOL)animated;
- (BOOL) isNetworkActive;

/*! Разрешение карты. Чисто для информации. Т.к. напрямую задать нельзя - лучше наверное не использовать */
- (double)resolution NS_DEPRECATED_IOS(4_0, 5_0);

- (void) markCoordinate: (CLLocationCoordinate2D) coordinate;
- (void) gotoCoordinate: (CLLocationCoordinate2D) coordinate;

- (void)startGPS;
- (void)stopGPS;
- (CLLocationCoordinate2D)userCoordinateFromGPS;
- (void)setGPSAutoPanMode:(AxMapGPSAutoPanMode)panMode;
- (CLLocationCoordinate2D)convertArcGisCoordinateX:(double)x y:(double)y;
- (CLLocationCoordinate2D)coordinateFromScreenPoint:(CGPoint)point;
- (CGPoint)screenPoint:(CLLocationCoordinate2D)coordinate;

// границы карты
@property (nonatomic) BOOL boundsControl;

- (CLRegion *)mapBounds;
- (void)updateMapBounds:(CLRegion *)newBounds;
- (BOOL)isCoordinateInMapBounds:(CLLocationCoordinate2D)coordinate;
- (BOOL)isCoordinateInMoscowRegion:(CLLocationCoordinate2D)coordinate;
+ (CLRegion *)moscowRegion;

/*Максимальный квадрат карты в координатах WGS84*/
@property (nonatomic, copy) AxLocationRect *extentRect;

/*! Видимая область карты в координатах WGS84, параметр используется для вычислений и не хранится,
    при присвоении значения вызывается метод - (void)setVisibleRect:(AxLocationRect *)locationRect animated:(BOOL)animated с параметром animated = NO */
@property (nonatomic, copy) AxLocationRect *visibleRect;

/*! Установка видимой области карты с возможной анимацией. */
- (void)setVisibleRect:(AxLocationRect *)locationRect animated:(BOOL)animated;

// масштаб карты

/*! Установка минимальных/максимальных значений для зума */
@property (nonatomic) double minScale;
@property (nonatomic) double maxScale;

/*! Зум на выбранную величину c центром в координатах WGS84 */
- (void)zoomToCoordinate:(CLLocationCoordinate2D) coordinate withScale:(double)scale animated:(BOOL)animated;

/*! Зум на прямоугольник с координатами WGS84 */
- (void)zoomToEnvelopeXmin:(double)xmin
                      ymin:(double)ymin
                      xmax:(double)xmax
                      ymax:(double)ymax
                  animated:(BOOL) animated;

// точки на карте

@property (nonatomic, readonly) NSSet *points;

- (void)addPoint:(id <AxMapPoint>)point;
- (void)addPoints:(id <NSFastEnumeration>)points;
- (void)removePoint:(id <AxMapPoint>)point;
- (void)removePoints:(id <NSFastEnumeration>)points;
- (void)refreshPoint:(id <AxMapPoint>)point;
- (void)refreshPoints:(id <NSFastEnumeration>)points;
- (void)clearUserPoints;
- (void)selectPoint:(id <AxMapPoint>)point withImage:(UIImage *)image otherPointsAlpha:(CGFloat)alpha;
- (void)deselectPoint;
- (void)zoomMapWithPoints:(id <NSFastEnumeration>)points expandFactor:(double)factor;

// построение маршрутов

- (id <AxMapRoute>)routeForPointsArray:(NSArray *)points;

- (id <AxMapRoute>) digiMapRouteWithPoints:(NSArray *)points
                                  language:(AxDigiMapLanguageType)langType
                                 routeType:(AxDigiMapRouteType)routeType
                                optimizeBy:(AxDigiMapOptimizeType)optimizeType
                          speedCalculation:(AxDigiMapSpeedCalculationType)speedCalcType
                               useBadRoads:(BOOL)useBadRoads;
- (void)drawRoute:(id <AxMapRoute>)route lineColor:(UIColor *)color lineWidth:(CGFloat)width showSourcePoints:(BOOL)showPoints;
- (void)removeRoute:(id <AxMapRoute>)route removeSourcePoints:(BOOL)removePoints;
- (void)zoomMapToRoute:(id <AxMapRoute>)route expandFactor:(double)factor;
- (void)selectStep:(NSUInteger)step inRoute:(id <AxMapRoute>)route lineColor:(UIColor *)color lineWidth:(CGFloat)width lineSolid:(BOOL)solid;
- (void)clearStepSelectionForRoute:(id <AxMapRoute>)route;
- (void)zoomToStep:(NSUInteger)step inRoute:(id <AxMapRoute>)route expandFactor:(double)factor;

// Новые объекты с полигонами

- (NSSet *)objectsOnMap;

- (void)showCalloutForMapObject:(AxMapObject *)mapObj animated:(BOOL)animated;
- (void)addMapObject:(AxMapObject *)mapObject;
- (void)addMapObjects:(id <NSFastEnumeration>)objects;
- (void)removeMapObject:(AxMapObject *)mapObject;
- (void)removeMapObjects:(id <NSFastEnumeration>)objects;

/*! Метод для AxMapObject'ов. Из других мест - не использовать */
- (void)didClicAtCalloutForMapObject:(AxMapObject *)mapObj;

@end
