//
//  AxNewMapMultiPolygonObject.h
//  AxUniversalMapKit
//
//  Created by Alexey Kochetov on 19.02.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

#import "AxNewAbstractMapShapeObject.h"
#import "AxNewGlobalConstants.h"

@interface AxNewMapMultiPolygonObject : AxNewAbstractMapShapeObject

- (void)updatePolygonWithFillColor:(UIColor *)polFillColor
                         lineColor:(UIColor *)polLineColor
                         lineWidth:(CGFloat)polLineWidth;

@end
