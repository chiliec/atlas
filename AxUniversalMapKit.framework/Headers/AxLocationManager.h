//
//  AxLocationManager.h
//  AxUniversalMap
//
//  Created by Balalaev Sergey on 7/15/13.
//  Copyright (c) 2013 Altarix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void (^AxOneLocationHandler) (CLLocation *location, NSString * messageError);

@interface AxLocationManager : NSObject <CLLocationManagerDelegate>
{
@protected
    CLLocationManager * manager;
    BOOL isOneCheck;
    BOOL isStarted;
    BOOL isStartedOne;
}

+ (AxLocationManager*) default;

//! Generate messageError, if location not found in this time. Default 30 sec
@property NSTimeInterval timeout;

//! For getLocationBlock the max time at update location. Default 0.2 sec
@property NSTimeInterval maxRefreshTime;

// Method for displaying authorization dialogue in iOS8+ (called authomatically during singletone initaialization)
// Don't forget to include in your app plist:

//	<key>NSLocationAlwaysUsageDescription</key>
//  <string></string>
//  <key>NSLocationWhenInUseUsageDescription</key>
//  <string></string>

- (void) requestAuthorization;

- (void) getOneLocationBlock: (AxOneLocationHandler) locationFound;

- (void) getLocationBlock: (AxOneLocationHandler) locationFound;

- (void) cancel;

@end