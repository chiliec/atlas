//
//  AxWebTiledLayer.h
//  AxUniversalMap
//
//  Created by Alexey Kochetov on 20.01.15.
//  Copyright (c) 2015 Altarix. All rights reserved.
//

/* Class is used to add custom map tiles as a layer to a map. The tiles are fetched through a
 template URL such as http://{subDomain}.host.com/{level}/{col}/{row}.png where {level} corresponds
 to a zoom level, and {col} and {row} represent tile column and row, respectively.*/

#import <Foundation/Foundation.h>

@class AGSSpatialReference, AGSWebTiledLayer;

@interface AxWebTiledLayer : NSObject

@property (nonatomic, strong, readonly) AGSWebTiledLayer *agsTiledLayer;
@property (nonatomic, strong, readonly) NSString *name;

+ (instancetype) webTiledLayerWithName: (NSString *) layerName
                             serverURL: (NSString *) urlString
                      spatialReference: (AGSSpatialReference *)spatialReference;

@end
