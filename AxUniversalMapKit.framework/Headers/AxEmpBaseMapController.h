//
//  MapViewController.h
//  MoscowAtlas
//
//  Created by Balalaev Sergey on 11/29/13.
//  Copyright (c) 2013 Altarix. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AxMap, NSManagedObjectContext;

@interface AxEmpBaseMapController : UIViewController

@property (nonatomic, strong) NSManagedObjectContext *mapObjectsContext;

@property (nonatomic, retain) IBOutlet AxMap * map;

@property (nonatomic) BOOL enablePanel;

- (void)showButtonPanel;
- (void)hideButtonPanel;

- (void) buttonPanelShow: (BOOL) show animated: (BOOL) animated;

- (IBAction)selectFilters;

- (IBAction)btModeClick:(UIButton *)sender;
- (IBAction)btShowMeClick:(id)sender;

- (IBAction)unwindWithResult:(UIStoryboardSegue*)sender;
- (IBAction)unwindWithCancel:(UIStoryboardSegue*)sender;

- (void) showProgressWithMessage:(NSString *)message;
- (void) hideProgress;

@end
