//
//  AxPolygonMapObject.h
//  AxUniversalMap
//
//  Created by Sergey Starukhin on 26.11.14.
//  Copyright (c) 2014 Altarix. All rights reserved.
//

#import <AxUniversalMapKit/AxMapObject.h>

@class UIColor;

@interface AxPolygonMapObject : AxMapObject

+ (UIColor *)defaultFillColor;
+ (UIColor *)defaultOutlineColor;

- (void)resetColorsToDefault;

- (void)updateFillColor:(UIColor *)fillColor outlineColor:(UIColor *)outlineColor;

@end
